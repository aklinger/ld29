package com.aa.ld29.log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * https://stackoverflow.com/questions/17853166/libgdx-desktop-logging-to-file
 *
 * Created by Toni on 29.04.2014.
 */
public class MyLogger {

    private static FileHandle logFile;
    private static final String newline = System.getProperty("line.separator");
    static {
        logFile = Gdx.files.local("log.txt");
        log("Started logging");
    }

    public static void log(String message, Exception ex){
        PrintStream write = new PrintStream(logFile.write(true));
        logFile.writeString(getCurrentTime()+" "+message+newline, true);
        ex.printStackTrace(write);
        logFile.writeString(newline, true);
    }
    public static void log(String message){
        logFile.writeString(getCurrentTime()+" "+message+newline, true);
    }
    public static String getCurrentTime(){
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
}

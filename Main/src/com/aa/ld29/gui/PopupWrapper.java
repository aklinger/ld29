package com.aa.ld29.gui;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by Toni on 14.07.2014.
 */
public class PopupWrapper implements InputProcessor{

    private Stage stage;
    private boolean visible;

    public PopupWrapper(Stage stage) {
        this.stage = stage;
        visible = true;
    }

    public void setVisible(boolean visible) {
        stage.getRoot().setVisible(visible);
        for(Actor actor : stage.getActors()){
            actor.setVisible(visible);
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        if(visible)
            return stage.keyDown(keycode);
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if(visible)
            return stage.keyUp(keycode);
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        if(visible)
            return stage.keyTyped(character);
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(visible)
            return stage.touchDown(screenX,screenY,pointer,button);
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(visible)
            return stage.touchUp(screenX,screenY,pointer,button);
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if(visible)
            return stage.touchDragged(screenX,screenY,pointer);
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if(visible)
            return stage.mouseMoved(screenX,screenY);
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        if(visible)
            return stage.scrolled(amount);
        return false;
    }
}

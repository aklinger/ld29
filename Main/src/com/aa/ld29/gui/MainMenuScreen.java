package com.aa.ld29.gui;

/**
 * Created by Toni on 14.04.2014.
 */

import com.aa.ld29.LD29Game;
import com.aa.ld29.dal.input.ControlSettings;
import com.aa.ld29.dal.input.InputAction;
import com.aa.ld29.gui.elements.JuicyButton;
import com.aa.ld29.gui.elements.Selector;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * The main menu screen.
 *
 * @author Toni
 */
public class MainMenuScreen extends BelowEarthScreen {

    private Label moveInstructionsLabel;
    private Label bombInstructionsLabel;
    private Label specialInstructionsLabel;

    private BitmapFont font;

    public MainMenuScreen(Skin skin, SpriteBatch batch, LD29Game game) {
        super("Main Menu", batch, game);
        addBackground("main_menu_bg");
        createTable(skin, batch);
        initFont();
    }

    private void initFont(){
        font = new BitmapFont(Gdx.files.internal("fonts/press_start_2p.fnt"),Gdx.files.internal("fonts/press_start_2p_0.png"),false);
        font.setColor(Color.WHITE);
        font.setFixedWidthGlyphs("0123456789");
        font.setScale(0.5f,0.5f);
    }

    private void createTable(final Skin skin, final SpriteBatch batch) {
        Table table = new Table(skin);
        table.setFillParent(true);
        stage.addActor(table);

        TextButton startGameButton = new JuicyButton("Start Singleplayer Game", skin);
        startGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                getGame().setScreen(new GameplayScreen(skin, batch, getGame(), false));
            }
        });
        TextButton startMultiplayerGameButton = new JuicyButton("Start Co-op Multiplayer Game", skin);
        startMultiplayerGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                getGame().setScreen(new GameplayScreen(skin, batch, getGame(), true));
            }
        });
        TextButton creditsButton = new JuicyButton("Credits", skin);
        creditsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                getGame().setScreen(new CreditsScreen(skin, batch, getGame()));
            }
        });
        TextButton optionsButton = new JuicyButton( "Options", skin);
        optionsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                getGame().setScreen(new OptionsScreen(skin, batch, getGame()));
            }
        });
        TextButton quitButton = new JuicyButton("Quit", skin);
        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });

        moveInstructionsLabel = new Label("",skin);
        bombInstructionsLabel = new Label("",skin);
        specialInstructionsLabel = new Label("",skin);

        MoveToAction action = new MoveToAction();
        action.setPosition(0, 0);
        action.setDuration(0.5f);
        table.setPosition(0, 500);
        table.addAction(action);

        float lineSpace = 5;

        table.add("Below Earth - LD29 'Beneath the Surface'").center().spaceBottom(12);
        table.row();
        table.add(startGameButton).size(300f, 60f).spaceBottom(10);
        table.row();
        table.add(startMultiplayerGameButton).size(280f, 50f).spaceBottom(10);
        table.row();
        table.add("Controls:").spaceBottom(lineSpace);
        table.row();
        table.add(moveInstructionsLabel).spaceBottom(lineSpace);
        table.row();
        table.add(bombInstructionsLabel).spaceBottom(lineSpace);
        table.row();
        table.add(specialInstructionsLabel).spaceBottom(lineSpace + 2);
        table.row();
        table.add("Made by Anton Klinger").spaceBottom(lineSpace);
        table.row();
        table.add(creditsButton).size(200f, 40f).spaceBottom(10);
        table.row();
        table.add( optionsButton ).size( 200f, 40f ).spaceBottom( 20 );
        table.row();
        table.add(quitButton).size(200f, 40f);

        selector.switchSelectedActor(startGameButton);
    }

    private void updateLabels(){
        ControlSettings controls = ControlSettings.getInstance();
        moveInstructionsLabel.setText("Move with "+ controls.getKeyName(InputAction.ACTION_MOVE_UP)+", "+
                controls.getKeyName(InputAction.ACTION_MOVE_DOWN)+", "+
                controls.getKeyName(InputAction.ACTION_MOVE_LEFT)+" and "+
                controls.getKeyName(InputAction.ACTION_MOVE_RIGHT)+".");
        bombInstructionsLabel.setText("Throw bombs with " + controls.getKeyName(InputAction.ACTION_THROW_BOMB));
        specialInstructionsLabel.setText("Throw special with " + controls.getKeyName(InputAction.ACTION_THROW_SPECIAL) + ". Cycle through specials with " + controls.getKeyName(InputAction.ACTION_SWITCH_SPECIAL));
    }

    @Override
    public void show() {
        super.show();
        updateLabels();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        stage.getBatch().begin();
        font.draw(stage.getBatch(),"v0.4.4a",10,10);
        stage.getBatch().end();
    }
}

package com.aa.ld29.gui;

import com.aa.ld29.LD29Game;
import com.aa.ld29.dal.*;
import com.aa.ld29.dal.audio.MusicManager;
import com.aa.ld29.dal.audio.SoundFX;
import com.aa.ld29.dal.audio.SoundManager;
import com.aa.ld29.dal.input.ControlSettings;
import com.aa.ld29.dal.input.InputAction;
import com.aa.ld29.gui.elements.JuicyButton;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by Toni on 09.07.2014.
 */
public class OptionsScreen extends BelowEarthScreen {

    private Label soundVolumeLabel;
    private Label musicVolumeLabel;
    private Label controlLabel;
    private boolean recording;
    private int currentKey;

    public OptionsScreen(Skin skin, SpriteBatch batch, LD29Game game) {
        super("Options Screen", batch, game);
        addBackground("main_menu_bg");
        createUIElements(skin);
    }

    @Override
    public void dispose() {
        super.dispose();
        BelowEarthPreferences.getInstance().saveSettings();
    }

    private void createUIElements(Skin skin) {
        Table table = new Table(skin);
        table.setFillParent(true);
        stage.addActor(table);
        TextButton quitButton = new JuicyButton("Back to Menu", skin);
        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                quitToMainMenu();
            }
        });
        TextButton controlsButton = new JuicyButton("Change Controls", skin);
        controlsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                recording = true;
                currentKey = 0;
                updateControlLabel();
            }
        });
        controlLabel = new Label(" ", skin);
        final CheckBox musicMute = new CheckBox("", skin);
        musicMute.addListener(new EventListener() {
            @Override
            public boolean handle(Event event) {
                MusicManager.getInstance().setEnabled(!musicMute.isChecked());
                return false;
            }
        });
        musicMute.setChecked(!MusicManager.getInstance().isEnabled());
        Slider soundVolumeSlider = new Slider(0f, 1f, 0.1f, false, skin);
        soundVolumeSlider.setValue(SoundManager.getInstance().getVolume());
        soundVolumeSlider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                float value = ((Slider) actor).getValue();
                SoundManager.getInstance().setVolume(value);
                updateSoundVolumeLabel();
                SoundManager.getInstance().play(SoundFX.HURT_3);
            }
        });
        Slider musicVolumeSlider = new Slider(0f, 1f, 0.1f, false, skin);
        musicVolumeSlider.setValue(MusicManager.getInstance().getVolume());
        musicVolumeSlider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                float value = ((Slider) actor).getValue();
                MusicManager.getInstance().setVolume(value);
                updateMusicVolumeLabel();
            }
        });
        soundVolumeLabel = new Label("", skin);
        updateSoundVolumeLabel();
        musicVolumeLabel = new Label("", skin);
        updateMusicVolumeLabel();

        float lineSpace = 9;

        table.add("Options").colspan(3).center().spaceBottom(20).spaceLeft(20).spaceRight(20);
        table.row();
        table.add("Sound Effect Volume").colspan(2).spaceBottom(lineSpace);
        table.row();
        table.add(soundVolumeLabel).spaceBottom(lineSpace);
        table.add(soundVolumeSlider).spaceBottom(lineSpace);
        table.row();
        table.add("Music Volume").colspan(2).spaceBottom(lineSpace);
        table.row();
        table.add(musicVolumeLabel).spaceBottom(lineSpace);
        table.add(musicVolumeSlider).spaceBottom(lineSpace);
        table.row();
        table.add("Mute Music").spaceBottom(lineSpace);
        table.add(musicMute).spaceBottom(lineSpace);
        table.row();
        table.add(controlsButton).colspan(3).spaceBottom(lineSpace);
        table.row();
        table.add(controlLabel).colspan(3).spaceBottom(lineSpace);
        table.row();
        table.add(quitButton).size(300f, 60f).colspan(3).spaceBottom(lineSpace);
    }

    /**
     * Updates the volume label next to the slider.
     */
    private void updateSoundVolumeLabel() {
        float volume = (SoundManager.getInstance().getVolume() * 100);
        soundVolumeLabel.setText("" + (Math.round(volume * 100) / 100) + " %");
    }

    private void updateMusicVolumeLabel() {
        float volume = (MusicManager.getInstance().getVolume() * 100);
        musicVolumeLabel.setText("" + (Math.round(volume * 100) / 100) + " %");
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
            quitToMainMenu();
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        if (recording) {
            ControlSettings.getInstance().setKey(InputAction.values()[currentKey], keycode);
            currentKey++;
            updateControlLabel();
            return true;
        }
        return false;
    }

    private void updateControlLabel() {
        if (currentKey < InputAction.values().length) {
            InputAction currentAction = InputAction.values()[currentKey];
            if(ControlSettings.getInstance().isAssigned(currentAction)) {
                controlLabel.setText("Press key for: '" + currentAction.getLabel() + "'. Current key is: '" + ControlSettings.getInstance().getKeyName(currentAction) + "'");
            }else{
                controlLabel.setText("Press key for: '" + currentAction.getLabel() + "'. Currently not assigned.");
            }
        } else {
            controlLabel.setText(" ");
            recording = false;
        }
    }
}

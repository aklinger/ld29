package com.aa.ld29.gui;

import com.aa.ld29.LD29Game;
import com.aa.ld29.dal.audio.MusicManager;
import com.aa.ld29.dal.TextureManager;
import com.aa.ld29.dal.input.ControlSettings;
import com.aa.ld29.dal.level.LevelDal;
import com.aa.ld29.dal.level.SimpleLevelLoader;
import com.aa.ld29.gui.elements.JuicyButton;
import com.aa.ld29.log.MyLogger;
import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.PlayerInfo;
import com.aa.ld29.logic.levels.CampaignInfo;
import com.aa.ld29.logic.levels.LevelLibrary;
import com.aa.ld29.logic.objects.Spelunker;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Toni on 14.04.2014.
 */
public class GameplayScreen extends BelowEarthScreen{

    //Attributes
    //Menu-Navigation
    private boolean pauseKeyPressed = false;
    private boolean pauseKeyPressed2 = false;
    private float pauseTimer = 0;
    private float quitDuration = 60*0.5f;
    private boolean paused = false;
    //Graphics
    private OrthographicCamera hudcamera;
    private final SpriteBatch batch;
    private ShapeRenderer shapeRenderer;
    private BitmapFont font;
    private static final int pixelSizeOnScreen = 2;
    private static final int GAME_WIDTH = Gdx.graphics.getWidth()/pixelSizeOnScreen;//400;//800;480
    private static final int GAME_HEIGHT = Gdx.graphics.getHeight()/pixelSizeOnScreen;//300;//600;320
    private boolean disposed = false;
    //Logic
    private boolean multiplayer;
    private EntityManager manager;
    private int startingHP = 8;
    private java.util.List<PlayerInfo> infos;
    private CampaignInfo campaignInfo;
    private int highScore = 0;
    private boolean dead = false;
    private boolean restarted = false;
    private float transitionDuration = 30;
    private float transitionTimer = 0;
    private boolean levelCompleted = false;
    //HUD-Graphics
    private Skin skin;
    private Table pauseScreen;
    private EditorManager editorManager;
    private PopupWrapper stageWrapper;
    //CONTROLLER
    private static java.util.List<Controller> gamepads;

    //Constructors
    public GameplayScreen(Skin skin, SpriteBatch batch, LD29Game game, boolean multiplayer) {
        super("GameplayScreen", batch, game);
        this.skin = skin;
        this.batch = batch;
        this.shapeRenderer = new ShapeRenderer();
        this.multiplayer = multiplayer;

        updateHudCam();
        initFont();
        initGamepads();
        initPauseScreen();

        campaignInfo = new CampaignInfo();
        campaignInfo.currentLevel = 1;
        infos = new ArrayList<>();
        if(multiplayer) {
            for (int i = 0; i < getGamepadCount() + 1; i++) {
                infos.add(new PlayerInfo(startingHP, 0, new int[]{10, 10, 10, 10, 10}));
            }
        }else{
            infos.add(new PlayerInfo(startingHP, 0, new int[]{10, 10, 10, 10, 10}));
        }
        initGameStuff();
    }

    private void initGamepads(){
        gamepads = new ArrayList<>();
        try {
            Array<Controller> controllers = Controllers.getControllers();
            for(Controller controller : controllers){
                gamepads.add(controller);
            }
        }catch (Exception ex){
            MyLogger.log("Failed setting up controllers.",ex);
        }
    }

    private void initFont(){
        font = new BitmapFont(Gdx.files.internal("fonts/press_start_2p.fnt"),Gdx.files.internal("fonts/press_start_2p_0.png"),false);
        font.setColor(Color.WHITE);
        font.setFixedWidthGlyphs("0123456789");
        font.setScale(0.5f, 1);
    }

    @Override
    protected void setInputProcessor() {
        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(ControlSettings.getInstance());
        inputMultiplexer.addProcessor(stageWrapper);
        inputMultiplexer.addProcessor(this);
        inputMultiplexer.addProcessor(selector);
        if(LD29Game.DEVELOPER_MODE) {
            inputMultiplexer.addProcessor(editorManager);
        }
        Gdx.input.setInputProcessor(inputMultiplexer);

        setSingleControllerListener();
    }

    private void initPauseScreen(){
        pauseScreen = new Table(skin);
        pauseScreen.setFillParent(true);
        stage.addActor(pauseScreen);
        TextButton quitButton = new JuicyButton("Back to Menu", skin);
        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                quitToMainMenu();
            }
        });
        TextButton resumeButton = new JuicyButton("Resume Game", skin);
        resumeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                paused = false;
            }
        });
        final CheckBox musicMute = new CheckBox("", skin);
        musicMute.addListener(new EventListener() {
            @Override
            public boolean handle(Event event) {
                MusicManager.getInstance().setEnabled(!musicMute.isChecked());
                return false;
            }
        });
        musicMute.setChecked(!MusicManager.getInstance().isEnabled());

        float lineSpace = 9;

        pauseScreen.add("Game Paused").colspan(2).center().spaceBottom(20);
        pauseScreen.row();
        pauseScreen.add(resumeButton).size(300f, 60f).colspan(2).spaceBottom(lineSpace);
        pauseScreen.row();
        pauseScreen.add("Mute Music").spaceBottom(lineSpace);
        pauseScreen.add(musicMute).spaceBottom(lineSpace);
        pauseScreen.row();
        pauseScreen.add(quitButton).size(200f, 50f).colspan(2).spaceBottom(lineSpace);
        if (LD29Game.DEVELOPER_MODE) {
            editorManager = new EditorManager(skin,manager);
            stage.addActor(editorManager.getEditorTable());
        }
        stageWrapper = new PopupWrapper(stage);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        updateHudCam();
    }

    private void updateHudCam(){
        hudcamera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        hudcamera.translate(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
        hudcamera.update();
    }

    private void initGameStuff(){
        if(!LevelLibrary.allLevelsFinished(campaignInfo.currentLevel)){
            manager = LevelLibrary.createLevel(campaignInfo.currentLevel,GAME_WIDTH,GAME_HEIGHT,multiplayer);
            manager.setImportantInfo(infos);
            if(editorManager != null) {
                editorManager.setEntityManager(manager);
            }
        }else{
            dispose();
            MyLogger.log("Completed Game with " + campaignInfo.restarts + " restarts.");
            getGame().setScreen(new WinningScreen(this.skin, batch, getGame(), campaignInfo, infos));
        }
    }

    private void restart(){
        for(PlayerInfo info : infos) {
            info.hp = startingHP;
        }
        initGameStuff();
        dead = false;
    }

    @Override
    public void dispose() {
        super.dispose();
        shapeRenderer.dispose();
        font.dispose();
        disposed = true;
    }

    //The important stuff
    @Override
    public void render(float delta) {
        super.render(delta);

        act();

        if(!disposed) {
            batch.setProjectionMatrix(manager.getPessimisticCamera().combined);
            batch.begin();
            renderGameArea(batch);
            batch.end();

            //renderMultiplayerOverlay();

            batch.setProjectionMatrix(hudcamera.combined);
            batch.begin();
            renderHud(batch);
            batch.end();

            fadeBlack((transitionTimer / transitionDuration));
            stage.draw();
        }
    }

    private void prepareOverlayMask(){
        //clear depth buffer with 1
        Gdx.gl.glClearDepthf(1f);
        Gdx.gl.glClear(GL20.GL_DEPTH_BUFFER_BIT);
        //set the function to LESS
        Gdx.gl.glDepthFunc(GL20.GL_LESS);
        //enable depth writing
        Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);
        //enable depth writing, disable RGBA color writing
        Gdx.gl.glDepthMask(true);
        Gdx.gl.glColorMask(false, false, false, false);
    }

    private void prepareOverlayRender(){
        //enable RGBA color writing
        // (SpriteBatch.begin() will disable depth mask)
        Gdx.gl.glColorMask(true, true, true, true);
        //Make sure testing is enabled.
        Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);
        //Now depth discards pixels outside our masked shapes
        Gdx.gl.glDepthFunc(GL20.GL_EQUAL);
    }

    private void cleanupOverlay(){
        //Disable depth test again because we draw 2D
        Gdx.gl.glDisable(GL20.GL_DEPTH_TEST);
    }

    private void renderMultiplayerOverlay(){
        java.util.List<Entity> players = manager.getPlayers();
        for (int i = 0; i < players.size(); i++) {
            Entity ent = players.get(i);

            float radius = 75;
            float displacementx = radius + i * radius*2;
            float displacementy = 75;

            prepareOverlayMask();
            // ---Draw mask shape---
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

            shapeRenderer.setColor(1f, 1f, 1f, 1f);
            shapeRenderer.circle(displacementx,displacementy, radius);
            shapeRenderer.end();

            // ---Draw sprite(s) to be masked---
            OrthographicCamera playerCamera = manager.getPlayerCamera(i);
            playerCamera.translate((GAME_WIDTH - displacementx)/2, ((GAME_HEIGHT) - displacementy)/2);
            playerCamera.update();

            batch.setProjectionMatrix(playerCamera.combined);
            batch.begin();

            prepareOverlayRender();

            //push to the batch
            manager.render(batch,ent.getCenter());
            //end/flush your batch
            batch.end();
        }

        cleanupOverlay();
    }

    private boolean isPauseButtonPressed(){
        return (Gdx.input.isKeyPressed(Input.Keys.ESCAPE) || (isGamepadAvailable(0) && getGamepad(0).getButton(7)));
    }

    private boolean isRestartButtonPressed(){
        return (Gdx.input.isKeyPressed(Input.Keys.R) || (isGamepadAvailable(0) && getGamepad(0).getButton(6)));
    }

    public void act(){
        stageWrapper.setVisible(paused);
        if(paused) {
            if(pauseKeyPressed2){
                if(isPauseButtonPressed()) {
                    pauseTimer++;
                }else{
                    paused = false;
                    pauseKeyPressed2 = false;
                    pauseKeyPressed = false;
                }
                if(pauseTimer > quitDuration){
                    quitToMainMenu();
                }
            }
            if (!pauseKeyPressed && isPauseButtonPressed()) {
                pauseKeyPressed2 = true;
                pauseKeyPressed = true;
                pauseTimer = 0;
            } else if (!isPauseButtonPressed()) {
                pauseKeyPressed = false;
            }
        }else {
            if (!pauseKeyPressed && isPauseButtonPressed()) {
                paused = true;
                pauseKeyPressed = true;
            }
            if (dead && !restarted && isRestartButtonPressed()) {
                restart();
                restarted = true;
            } else {
                if (!isRestartButtonPressed()) {
                    restarted = false;
                }
            }

            manager.act();
            if (manager.isVictory()) {
                levelCompleted = true;
            }
            if(levelCompleted){
                transitionTimer++;
                if(transitionTimer>=transitionDuration){
                    changeLevel();
                    levelCompleted = false;
                }
            }else{
                if(transitionTimer > 0) {
                    transitionTimer--;
                }
            }
        }
        ControlSettings.getInstance().actFrame();
    }
    private void changeLevel(){
        MyLogger.log("Completed Level " + campaignInfo.currentLevel);
        for (int i = 0; i < infos.size(); i++) {
            Spelunker player = (Spelunker) manager.getPlayers().get(i);
            PlayerInfo info = infos.get(i);
            MyLogger.log("Player"+i+" {Remaining HP: "+ info.hp + "; Remaining Ammo: "+ Arrays.toString(info.specialAmmo)+";}");
            if(player.getHealth()<=0){
                info.deaths++;
            }
            info.hp = player.getHealth() + 1;
            info.score = manager.getScore();
            info.specialAmmo = player.getSpecialAmmo();
            info.currentSpecial = player.getCurrentSpecial();
        }
        campaignInfo.currentLevel++;
        initGameStuff();
    }
    public void renderGameArea(SpriteBatch batch){
        manager.render(batch);
    }
    public void renderHud(SpriteBatch batch){
        if(manager.getPlayers().size() == 1) {
            Entity p = manager.getPlayers().get(0);
            for (int i = 0; i < p.getHealth(); i++) {
                batch.draw(TextureManager.getInstance().getTexture("heart"), 20 + i * 20, 25, 32, 32);
            }
            Spelunker player = (Spelunker) manager.getPlayers().get(0);
            batch.draw(TextureManager.getInstance().getTexture(Spelunker.getSpecialIcon(player.getCurrentSpecial())), 25, 5);
            font.draw(batch, "x" + player.getCurrentSpecialAmmo(), 50, 20);
        }
        font.draw(batch,"Level: "+ campaignInfo.currentLevel,110,20);
        if(manager.getAliveTargets().size() <= 0){
            if(!dead) {
                campaignInfo.restarts++;
                for(PlayerInfo info : infos){
                    info.deaths++;
                }
                MyLogger.log("Died "+campaignInfo.restarts+" times.");
                dead = true;
            }
            String deathText = "You are dead. Press R to restart the level.";
            BitmapFont.TextBounds bounds = font.getBounds(deathText);
            font.draw(batch,deathText,Gdx.graphics.getWidth()/2-bounds.width/2,Gdx.graphics.getHeight()/2-bounds.height/2);
        }

        if(manager.getHelpText() != null){
            BitmapFont.TextBounds bounds = font.getBounds(manager.getHelpText());
            font.draw(batch,manager.getHelpText(),Gdx.graphics.getWidth()/2-bounds.width/2,Gdx.graphics.getHeight()/(4f)-bounds.height/2);
        }
    }

    public static boolean isGamepadAvailable(int index){
        return index >= 0 && gamepads.size()-1 >= index && gamepads.get(index) != null;
    }

    public static Controller getGamepad(int index){
        return gamepads.get(index);
    }

    public static int getGamepadCount(){
        return gamepads.size();
    }

    @Override
    public boolean keyDown(int keycode) {
        if(LD29Game.DEVELOPER_MODE) {
            if(keycode == Input.Keys.NUM_0){
                campaignInfo.currentLevel++;
                MyLogger.log("Switched to level "+campaignInfo.currentLevel);
                for (int i = 0; i < manager.getPlayers().size(); i++) {
                    Spelunker player = (Spelunker) manager.getPlayers().get(i);
                    PlayerInfo info = infos.get(i);
                    info.hp = player.getHealth();
                    info.score = manager.getScore();
                    info.specialAmmo = player.getSpecialAmmo();
                    info.currentSpecial = player.getCurrentSpecial();
                }
                initGameStuff();
            }else if(keycode == Input.Keys.NUM_9){
                campaignInfo.currentLevel--;
                MyLogger.log("Switched to level "+campaignInfo.currentLevel);
                for (int i = 0; i < manager.getPlayers().size(); i++) {
                    Spelunker player = (Spelunker) manager.getPlayers().get(i);
                    PlayerInfo info = infos.get(i);
                    info.hp = player.getHealth();
                    info.score = manager.getScore();
                    info.specialAmmo = player.getSpecialAmmo();
                    info.currentSpecial = player.getCurrentSpecial();
                }
                initGameStuff();
            }
            if (keycode == Input.Keys.NUM_1) {
                LevelDal dal = new SimpleLevelLoader(multiplayer);
                try {
                    dal.saveToFile(manager, Gdx.files.external("temp.lvl"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            } else if (keycode == Input.Keys.NUM_2) {
                LevelDal dal = new SimpleLevelLoader(multiplayer);
                try {
                    EntityManager loaded = dal.loadFromFile(Gdx.files.external("temp.lvl"));
                    for(PlayerInfo info : infos) {
                        info.hp = startingHP;
                    }
                    manager = loaded;
                    manager.setImportantInfo(infos);
                    if (editorManager != null) {
                        editorManager.setEntityManager(manager);
                    }
                    dead = false;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            }
        }
        return false;
    }

    private void fadeBlack(float alpha){
        if(alpha > 0) {
            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

            shapeRenderer.setProjectionMatrix(hudcamera.combined);

            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(0, 0, 0, alpha);
            shapeRenderer.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            shapeRenderer.end();

            Gdx.gl.glDisable(GL20.GL_BLEND);
        }
    }
}

package com.aa.ld29.gui;

import com.aa.ld29.LD29Game;
import com.aa.ld29.dal.TextureManager;
import com.aa.ld29.gui.elements.JuicyButton;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by Toni on 28.04.2014.
 */
public class CreditsScreen extends BelowEarthScreen {

    public CreditsScreen(Skin skin, SpriteBatch batch, LD29Game game) {
        super("Credits Screen", batch, game);
        addBackground("main_menu_bg");
        createTable(skin,batch);
    }

    //Methods
    private void createTable(final Skin skin, final SpriteBatch batch){
        Table table = new Table(skin);
        table.setFillParent(true);
        stage.addActor(table);
        TextButton quitButton = new JuicyButton( "Back to Menu", skin );
        quitButton.addListener( new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                quitToMainMenu();
            }
        } );

        float lineSpace = 8;

        table.add("LD29 'Beneath the Surface' - Below Earth Credits").colspan(2).center().spaceBottom(20);
        table.row();
        table.add("Original made in 72 hours.").colspan(2).spaceBottom( lineSpace );
        table.row();
        addCredits(table,lineSpace);
        table.add("Have fun blowing yourself up!").colspan(2).spaceBottom( lineSpace );
        table.row();
        table.add( quitButton ).size( 300f, 60f ).colspan(2).spaceBottom( lineSpace );
    }

    public static void addCredits(Table table, float lineSpace){
        table.add("Programming and Artwork by Anton Klinger").colspan(2).spaceBottom( lineSpace );
        table.row();
        table.add("Special thanks to:").colspan(2).spaceBottom( lineSpace * 2);
        table.row();
        table.add("Vinzenz Brodschneider").space( lineSpace );
        table.add("Horst Petschenig").space( lineSpace );
        table.row();
        table.add("Alexander Glueck").space( lineSpace );
        table.add("Florian Wagner").space( lineSpace );
        table.row();
        table.add("Manuel Steinermayr").space( lineSpace );
        table.add("and of course, you!").space( lineSpace *2);
        table.row();
    }
}

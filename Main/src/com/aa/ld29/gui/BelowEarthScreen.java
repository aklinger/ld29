package com.aa.ld29.gui;

import com.aa.ld29.LD29Game;
import com.aa.ld29.dal.TextureManager;
import com.aa.ld29.dal.input.ControlSettings;
import com.aa.ld29.gui.elements.Selector;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Created by Toni on 15.07.2014.
 */
public class BelowEarthScreen extends AbstractScreen {

    private final LD29Game game;

    protected Selector selector;

    private static ControllerListener one;

    public BelowEarthScreen(String screenName, SpriteBatch batch, LD29Game game) {
        super(screenName, batch);
        this.game = game;
        this.selector = new Selector(stage);
    }

    @Override
    protected void setInputProcessor(){
        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(stage);
        inputMultiplexer.addProcessor(this);
        inputMultiplexer.addProcessor(selector);
        Gdx.input.setInputProcessor(inputMultiplexer);

        setSingleControllerListener();
    }

    public void setSingleControllerListener(){
        if(one != null) {
            Controllers.removeListener(one);
        }
        Controllers.addListener(selector);
        one = selector;
    }

    protected void quitToMainMenu() {
        this.dispose();
        game.goToMainMenuScreen();
    }

    protected void addBackground(String bgImage) {
        Texture tex = TextureManager.getInstance().getTexture(bgImage);

        Image splashImage = new Image(tex);
        splashImage.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        splashImage.setColor(1, 1, 1, 1);
        splashImage.setFillParent(true);

        stage.addActor(splashImage);
    }

    public LD29Game getGame() {
        return game;
    }
}

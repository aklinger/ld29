package com.aa.ld29.gui;

import com.aa.ld29.LD29Game;
import com.aa.ld29.dal.TextureManager;
import com.aa.ld29.logic.PlayerInfo;
import com.aa.ld29.logic.levels.CampaignInfo;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.List;

/**
 * Created by Toni on 28.04.2014.
 */
public class WinningScreen extends BelowEarthScreen{

    private CampaignInfo campaignInfo;
    private List<PlayerInfo> infos;

    public WinningScreen(Skin skin, SpriteBatch batch, LD29Game game, CampaignInfo campaignInfo, List<PlayerInfo> infos) {
        super("Winning Screen", batch, game);
        addBackground("main_menu_bg");
        this.infos = infos;
        this.campaignInfo = campaignInfo;
        createTable(skin,batch);
    }

    //Methods
    private void createTable(final Skin skin, final SpriteBatch batch){
        Table table = new Table(skin);
        table.setFillParent(true);
        stage.addActor(table);
        TextButton quitButton = new TextButton( "Back to Menu", skin );
        quitButton.addListener( new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                quitToMainMenu();
            }
        } );

        float lineSpace = 8;

        table.add("Congratulations!! You are a winner!").colspan(2).center().spaceBottom(20);
        table.row();
        table.add("You only died "+getTotalDeaths()+" times!").colspan(2).spaceBottom(lineSpace);
        table.row();
        CreditsScreen.addCredits(table, lineSpace);
        table.add("You are a really awesome person.").colspan(2).spaceBottom(lineSpace);
        table.row();
        table.add("I would give you something besides silly little games, if I could, but I can't, which is really sad... :(").colspan(2).spaceBottom( lineSpace );
        table.row();
        table.add( quitButton ).colspan(2).size( 300f, 60f ).spaceBottom( lineSpace );
    }

    private int getTotalDeaths(){
        int sum = 0;
        for(PlayerInfo info : infos){
            sum += info.deaths;
        }
        return sum;
    }
}

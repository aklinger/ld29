package com.aa.ld29.gui.elements;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

/**
 * Created by Toni on 25.05.2014.
 */
public class JuicyButton extends TextButton {

    private float bonusScale = 0;
    private float deltaScale = 0;
    private float scaleAcceleration = 0.2f;
    private float scaleFriction = 0.8f;
    private float targetScale = 0.3f;

    public JuicyButton(String text, Skin skin) {
        super(text, skin);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if(isOver()){
            deltaScale += (targetScale - bonusScale)* scaleAcceleration;
        }else{
            deltaScale += (-bonusScale)* scaleAcceleration;
        }
        deltaScale *= scaleFriction;
        bonusScale += deltaScale;

        float realScale = bonusScale + 1;
        Matrix4 matrix4 = batch.getTransformMatrix();
        Matrix4 original = matrix4.cpy();
        matrix4.translate((getX()+getWidth()/2),(getY()+getHeight()/2),0);
        matrix4.scale(realScale,realScale,1);
        matrix4.translate(-getX()-getWidth()/2,-getY()-getHeight()/2,0);
        batch.setTransformMatrix(matrix4);
        super.draw(batch, parentAlpha);
        batch.setTransformMatrix(original);
    }

    public void setScaleAcceleration(float scaleAcceleration) {
        this.scaleAcceleration = scaleAcceleration;
    }

    public void setScaleFriction(float scaleFriction) {
        this.scaleFriction = scaleFriction;
    }

    public void setTargetScale(float targetScale) {
        this.targetScale = targetScale;
    }
}
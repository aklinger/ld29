package com.aa.ld29.gui.elements;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pools;

/**
 * Created by Toni on 20.08.2014.
 */
public class Selector extends InputAdapter implements ControllerListener{
    private Stage stage;
    private Actor selectedActor;
    private ShapeRenderer shapeRenderer;

    private int lastX;
    private int lastY;

    public Selector(Stage stage){
        this.stage = stage;
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setColor(Color.YELLOW);
    }

    public void dispose(){
        shapeRenderer.dispose();
    }

    public void render(){
        if(selectedActor != null) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.rect(selectedActor.getX(), selectedActor.getY(), selectedActor.getWidth(), selectedActor.getHeight());
            shapeRenderer.rect(selectedActor.getX()+1,selectedActor.getY()+1,selectedActor.getWidth()-2,selectedActor.getHeight()-2);
            shapeRenderer.rect(selectedActor.getX()-1,selectedActor.getY()-1,selectedActor.getWidth()+2,selectedActor.getHeight()+2);
            shapeRenderer.rect(lastX-1,lastY-1,3,3);
            shapeRenderer.end();
        }
    }

    public void switchSelectedActor(Actor newSelectedActor){
        if(this.selectedActor != null){
            unselectButton(this.selectedActor);
        }
        this.selectedActor = newSelectedActor;
        if(this.selectedActor != null){
            selectButton(this.selectedActor);
        }
    }

    public void pressButton() {
        if(selectedActor instanceof Button){
            clickButton(selectedActor);
            releaseButton(selectedActor);
        }
    }

    /**
     * Simulate button click down.
     * @param button
     * @return
     */
    private boolean clickButton(Actor button) {
        InputEvent event = Pools.obtain(InputEvent.class);
        event.setType(InputEvent.Type.touchDown);
        event.setButton(Input.Buttons.LEFT);

        button.fire(event);
        boolean handled = event.isHandled();
        Pools.free(event);
        return handled;
    }

    /**
     * Simulate button click release.
     * @param button
     * @return
     */
    private boolean releaseButton(Actor button) {
        InputEvent event = Pools.obtain(InputEvent.class);
        event.setType(InputEvent.Type.touchUp);
        event.setButton(Input.Buttons.LEFT);

        button.fire(event);
        boolean handled = event.isHandled();
        Pools.free(event);
        return handled;
    }
    /**
     * Simulate mousing over a button.
     * @param button
     * @return
     */
    private boolean selectButton(Actor button) {
        InputEvent event = Pools.obtain(InputEvent.class);
        event.setType(InputEvent.Type.enter);

        button.fire(event);
        boolean handled = event.isHandled();
        Pools.free(event);
        return handled;
    }

    /**
     * Simulate mousing off of a button.
     * @param button
     * @return
     */
    private boolean unselectButton(Actor button) {
        InputEvent event = Pools.obtain(InputEvent.class);
        event.setType(InputEvent.Type.exit);

        button.fire(event);
        boolean handled = event.isHandled();
        Pools.free(event);
        return handled;
    }

    public boolean moveMouse(int screenX, int screenY){
        Array<Actor> validActors = findValidActorsFromStage(stage);
        if(validActors.size == 0){
            switchSelectedActor(null);
        }else {
            lastX = screenX;
            lastY = screenY;
            for (Actor validActor : validActors) {
                /*Actor hitActor = validActor.hit(screenX,screenY,false);
                if(hitActor != null){
                    switchSelectedActor(hitActor);
                    break;
                }*/
                if(hit(screenX,screenY,validActor)){
                    switchSelectedActor(validActor);
                    return true;
                }
            }
        }
        return false;
    }

    private boolean hit(int screenX, int screenY, Actor actor){
        Vector2 localCoordinates = actor.stageToLocalCoordinates(new Vector2(screenX, screenY));
        return localCoordinates.x >= 0 && localCoordinates.x < actor.getWidth() && localCoordinates.y >= 0 && localCoordinates.y < actor.getHeight();
    }

    public void selectNextActor(Stage stage){
        Array<Actor> validActors = findValidActorsFromStage(stage);
        if(validActors.size == 0){
            switchSelectedActor(null);
        }else {
            int indexOf = validActors.indexOf(selectedActor, true);
            indexOf++;
            if (indexOf >= validActors.size) {
                indexOf = 0;
            }
            switchSelectedActor(validActors.get(indexOf));
        }
    }

    public void selectPreviousActor(Stage stage){
        Array<Actor> validActors = findValidActorsFromStage(stage);
        if(validActors.size == 0){
            switchSelectedActor(null);
        }else {
            int indexOf = validActors.indexOf(selectedActor, true);
            indexOf--;
            if (indexOf < 0) {
                indexOf = validActors.size - 1;
            }
            switchSelectedActor(validActors.get(indexOf));
        }
    }

    private Array<Actor> findValidActorsFromStage(Stage stage){
        Array<Actor> actors = new Array<>();
        for (Actor actor : stage.getActors()){
            actors.addAll(findAllValidActorsFromActor(actor));
        }
        return actors;
    }

    private Array<Actor> findAllValidActorsFromActor(Actor actor){
        Array<Actor> actors = new Array<>();
        if(actor.isVisible()) {
            if (actor instanceof Button) {
                actors.add(actor);
            } else if (actor instanceof Table) {
                for (Cell cell : ((Table)actor).getCells()) {
                    actors.addAll(findAllValidActorsFromActor(cell.getActor()));
                }
            }
        }
        return actors;
    }

    @Override
    public boolean keyDown(int keycode) {
        if(keycode == Input.Keys.DOWN){
            selectNextActor(stage);
            return true;
        }else if(keycode == Input.Keys.UP){
            selectPreviousActor(stage);
            return true;
        }else if(keycode == Input.Keys.LEFT){
            selectPreviousActor(stage);
            return true;
        }else if(keycode == Input.Keys.RIGHT){
            selectNextActor(stage);
            return true;
        }else if(keycode == Input.Keys.ENTER){
            pressButton();
            return true;
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        Vector3 unproject = stage.getCamera().unproject(new Vector3(screenX, screenY, 0));
        moveMouse((int) unproject.x, (int) unproject.y);
        return false;
    }

    @Override
    public void connected(Controller controller) {

    }

    @Override
    public void disconnected(Controller controller) {

    }

    @Override
    public boolean buttonDown(Controller controller, int buttonCode) {
        if(buttonCode == 0){
            pressButton();
        }
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int buttonCode) {
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int axisCode, float value) {
        return false;
    }

    @Override
    public boolean povMoved(Controller controller, int povCode, PovDirection value) {
        if(value == PovDirection.south){
            selectNextActor(stage);
            return true;
        }else if(value == PovDirection.north){
            selectPreviousActor(stage);
            return true;
        }else if(value == PovDirection.west){
            selectPreviousActor(stage);
            return true;
        }else if(value == PovDirection.east){
            selectNextActor(stage);
            return true;
        }
        return false;
    }

    @Override
    public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
        return false;
    }
}

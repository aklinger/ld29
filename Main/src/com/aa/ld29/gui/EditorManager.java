package com.aa.ld29.gui;

import com.aa.ld29.gui.elements.JuicyButton;
import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityFactory;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.tile.Tile;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by Toni on 14.07.2014.
 */
public class EditorManager implements InputProcessor{

    private ToolMode currentMode = ToolMode.VIEW;
    private int item = -1;

    private Table editorTable;
    private Table contentTable;
    private EntityManager manager;

    public EditorManager(Skin skin, EntityManager manager){
        createTable(skin);
        this.manager = manager;
    }

    private void createTable(final Skin skin){
        editorTable = new Table(skin);
        editorTable.setX(100+5);
        editorTable.setY(600/2);
        //editorTable.setFillParent(true);
        ButtonGroup bGroup = new ButtonGroup();
        Table toolTable = new Table();
        for (int i = 0; i < ToolMode.values().length; i++) {
            final int index = i;
            JuicyButton toolModeButton = new JuicyButton(ToolMode.values()[i].getLabel(), skin);
            toolModeButton.setScaleAcceleration(0.1f);
            bGroup.add(toolModeButton);//TODO FIND OUT HOW TO USE BUTTON GROUP
            toolModeButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    setCurrentMode(ToolMode.values()[index]);
                    contentTable.clear();
                    for (int j = 0; j < ToolMode.values()[index].getItems().length; j++) {
                        final int currentItem = ToolMode.values()[index].getItems()[j];
                        JuicyButton itemButton = new JuicyButton("Item: "+currentItem, skin);
                        itemButton.setScaleAcceleration(0.05f);
                        itemButton.addListener(new ClickListener() {
                            @Override
                            public void clicked(InputEvent event, float x, float y) {
                                item = currentItem;
                            }
                        });
                        contentTable.add(itemButton).size(80,40);
                        contentTable.row();
                    }

                }
            });
            toolTable.add(toolModeButton).size(120,50);
            toolTable.row();
        }
        contentTable = new Table(skin);
        editorTable.columnDefaults(2);
        editorTable.add(toolTable).width(120);
        editorTable.add(contentTable).width(80);
    }

    public void setCurrentMode(ToolMode currentMode) {
        this.currentMode = currentMode;
    }

    public Table getEditorTable() {
        return editorTable;
    }

    public void setEntityManager(EntityManager manager) {
        this.manager = manager;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(button == Input.Buttons.LEFT) {
            try {
                Vector3 unproject;
                switch (currentMode) {
                    case EDIT_TILE:
                        // create rocks
                        unproject = manager.getMedianCamera().unproject(new Vector3(screenX, screenY, 0));
                        Vector2 index = manager.getTileManager().getIndexAt(unproject.x, unproject.y);
                        Tile t;
                        switch (item) {
                            case 0:
                            default:
                                t = Tile.FLOOR;
                                break;
                            case 1:
                                t = Tile.WALL;
                                break;
                            case 2:
                                t = Tile.BEDROCK;
                                break;
                        }
                        manager.getTileManager().setTile((int) index.x, (int) index.y, t);
                        break;
                    case SPAWN_GOODIE:
                        // spawn bonus
                        unproject = manager.getMedianCamera().unproject(new Vector3(screenX, screenY, 0));
                        Entity goodie = EntityFactory.createGoodie(unproject.x, unproject.y, item);
                        manager.spawnEntity(goodie);
                        break;
                    case SPAWN_BOMB:
                        // spawn bomb
                        unproject = manager.getMedianCamera().unproject(new Vector3(screenX, screenY, 0));
                        Entity bomb = EntityFactory.createBomb(unproject.x, unproject.y,null, item);
                        manager.spawnEntity(bomb);
                        break;
                    case SPAWN_ENEMIES:
                        // spawn enemy
                        unproject = manager.getMedianCamera().unproject(new Vector3(screenX, screenY, 0));
                        Entity enemy = EntityFactory.createEnemy(unproject.x, unproject.y, item, false);
                        manager.spawnEntity(enemy);
                        break;
                    case SPAWN_BOSS:
                        // spawn boss
                        unproject = manager.getMedianCamera().unproject(new Vector3(screenX, screenY, 0));
                        Entity boss = EntityFactory.createEnemy(unproject.x, unproject.y, item, true);
                        manager.spawnEntity(boss);
                        break;
                    case VIEW:
                        //move camera
                        break;
                }
            }catch(RuntimeException ex){
                ex.printStackTrace();
            }
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    private enum ToolMode{
        VIEW("View",new int[]{}),
        EDIT_TILE("Edit Tiles",new int[]{0,1,2}),
        SPAWN_GOODIE("Spawn Bonus", new int[]{1,2,3,4}),
        SPAWN_BOMB("Spawn Bombs",new int[]{1,2,3,4,5,6,7}),
        SPAWN_ENEMIES("Spawn Enemies", new int[]{1,2,3,4,5,6}),
        SPAWN_BOSS("Spawn Boss", new int[]{1,2,3,4,5,6});

        private String label;
        private int[] items;
        private ToolMode(String label, int[] items){
            this.label = label;
            this.items = items;
        }
        public String getLabel() {
            return label;
        }

        public int[] getItems() {
            return items;
        }
    }
}

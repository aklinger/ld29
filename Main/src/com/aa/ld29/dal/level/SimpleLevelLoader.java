package com.aa.ld29.dal.level;

import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.levels.LevelLibrary;
import com.aa.ld29.logic.objects.Door;
import com.aa.ld29.logic.tile.Tile;
import com.aa.ld29.logic.tile.TileManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Toni on 26.07.2014.
 */
public class SimpleLevelLoader implements LevelDal {

    private boolean multiplayer;

    public SimpleLevelLoader(boolean multiplayer) {
        this.multiplayer = multiplayer;
    }

    public void saveToFile(EntityManager manager, FileHandle fh) throws IOException{
        Writer writer = fh.writer(false);
        TileManager tileManager = manager.getTileManager();
        Entity player = manager.getPlayers().get(0);
        Vector2 playerPosition = tileManager.getIndexAt(player.getCenter().x, player.getCenter().y);
        Entity goal = null;
        for(Entity ent : manager.getEntities()){
            if(ent instanceof Door){
                goal = ent;
            }
        }
        Vector2 goalPosition = new Vector2(-1,-1);
        if(goal != null){
            goalPosition = tileManager.getIndexAt(goal.getCenter().x,goal.getCenter().y);
        }
        for (int j = tileManager.getMapHeight()-1; j >= 0; j--) {
            for (int i = 0; i < tileManager.getMapWidth(); i++) {
                if(playerPosition.x == i && playerPosition.y == j){
                    writer.write("s");
                }else if(goalPosition.x == i && goalPosition.y == j){
                    writer.write("g");
                }else{
                    Tile t = tileManager.getTile(i, j);
                    if (t.equals(Tile.FLOOR)) {
                        writer.write("f");
                    } else if (t.equals(Tile.WALL)) {
                        writer.write("w");
                    } else if (t.equals(Tile.BEDROCK)) {
                        writer.write("i");
                    }
                }
            }
            writer.write("\r\n");
        }
        writer.flush();
    }

    public EntityManager loadFromFile(FileHandle fh) throws IOException {
        BufferedReader reader = fh.reader(256);
        String line;
        List<String> lines = new ArrayList<>();
        while ((line = reader.readLine()) != null) {
            lines.add(line);
        }
        reader.close();
        //Create the level
        int levelWidth = lines.get(0).length();
        int levelHeight = lines.size();
        TileManager tileManager = new TileManager(levelWidth, levelHeight, LevelLibrary.tileSize);
        tileManager.fillMapWith(Tile.FLOOR);
        EntityManager manager = new EntityManager(Gdx.graphics.getWidth(),Gdx.graphics.getHeight(),multiplayer);
        manager.setTileManager(tileManager);
        for (int j = 0; j < lines.size(); j++) {
            line = lines.get(lines.size() - 1 - j);
            for (int i = 0; i < line.length(); i++) {
                char code = line.charAt(i);
                switch (code) {
                    case 'w':
                        tileManager.setTile(i, j, Tile.WALL);
                        break;
                    case 'f':
                        tileManager.setTile(i, j, Tile.FLOOR);
                        break;
                    case 's':
                        manager.createPlayer(i, j, false);
                        break;
                    case 'g':
                        manager.createExit(i, j);
                        break;
                    case 't':
                        manager.createEnemy(i, j, 1);
                        break;
                    case 'd':
                        manager.createEnemy(i, j, 2);
                        break;
                    case 'm':
                        manager.createEnemy(i, j, 3);
                        break;
                    case 'q':
                        manager.createEnemy(i, j, 4);
                        break;
                    case '+':
                        manager.createGoodie(i, j, 2, false);
                        break;
                    case 'a':
                        manager.createGoodie(i, j, 3, false);
                        break;
                    case 'e':
                        manager.createEnemy(i, j, (int) (Math.random() * 5) + 1);
                        break;
                    case 'x':
                        if (Math.random() < 0.5) {
                            tileManager.setTile(i, j, Tile.WALL);
                        } else {
                            tileManager.setTile(i, j, Tile.FLOOR);
                        }
                        break;
                    case 'i':
                        tileManager.setTile(i, j, Tile.BEDROCK);
                        break;
                }
            }
        }
        return manager;
    }
}

package com.aa.ld29.dal.level;

import com.aa.ld29.logic.EntityManager;
import com.badlogic.gdx.files.FileHandle;

import java.io.IOException;

/**
 * Created by Toni on 26.07.2014.
 */
public interface LevelDal {

    EntityManager loadFromFile(FileHandle fh) throws IOException;

    void saveToFile(EntityManager manager, FileHandle fh) throws IOException;

}

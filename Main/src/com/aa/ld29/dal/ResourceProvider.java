package com.aa.ld29.dal;

import com.badlogic.gdx.graphics.Texture;

/**
 * An abstract way of accessing Resources.
 * <p/>
 * Created by Toni on 12.07.2014.
 */
public interface ResourceProvider {
    //Methods
    public Texture getTexture(String name);
}
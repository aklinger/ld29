package com.aa.ld29.dal.input;

/**
 * Created by Toni on 12.07.2014.
 */
public class KeyState {
    private boolean pressedLastFrame;
    private boolean pressedCurrentFrame;

    public boolean isDown() {
        return pressedCurrentFrame;
    }

    public void actFrame(){
        pressedLastFrame = pressedCurrentFrame;
    }

    public void press(){
        pressedCurrentFrame = true;
    }

    public void release(){
        pressedCurrentFrame = false;
    }

    public boolean isPressed() {
        return pressedCurrentFrame && !pressedLastFrame;
    }

    public boolean isReleased() {
        return !pressedCurrentFrame && pressedLastFrame;
    }
}

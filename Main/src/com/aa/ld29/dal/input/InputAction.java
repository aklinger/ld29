package com.aa.ld29.dal.input;

/**
 * Created by Toni on 12.07.2014.
 */
public enum InputAction {
    ACTION_MOVE_UP("action.move.up","Move Up"), ACTION_MOVE_DOWN("action.move.down","Move Down"),
    ACTION_MOVE_LEFT("action.move.left","Move Left"),ACTION_MOVE_RIGHT("action.move.right","Move Right"),
    ACTION_THROW_BOMB("action.throw.bomb","Throw Bomb"), ACTION_THROW_SPECIAL("action.throw.special","Throw Special"),
    ACTION_SWITCH_SPECIAL("action.switch.special","Switch Special");

    private String code;
    private String label;

    private InputAction(String code, String label){
        this.code = code;
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }
}

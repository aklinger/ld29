package com.aa.ld29.dal.input;

/**
 * Created by Toni on 12.07.2014.
 */
public class KeyMapping extends KeyState{
    private int key;

    public KeyMapping(int key) {
        this.key = key;
    }

    public int getKey() {
        return key;
    }
}

package com.aa.ld29.dal.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Toni on 12.07.2014.
 */
public class ControlSettings implements InputProcessor{

    private Map<InputAction,KeyMapping> keyMap;

    private static final ControlSettings instance;

    static{
        instance = new ControlSettings();
    }

    public static ControlSettings getInstance(){
        return instance;
    }

    private ControlSettings(){
        keyMap = new HashMap<>();
        keyMap.put(InputAction.ACTION_MOVE_LEFT, new KeyMapping(Input.Keys.A));
        keyMap.put(InputAction.ACTION_MOVE_RIGHT, new KeyMapping(Input.Keys.D));
        keyMap.put(InputAction.ACTION_MOVE_UP, new KeyMapping(Input.Keys.W));
        keyMap.put(InputAction.ACTION_MOVE_DOWN, new KeyMapping(Input.Keys.S));
        keyMap.put(InputAction.ACTION_THROW_BOMB, new KeyMapping(Input.Keys.J));
        keyMap.put(InputAction.ACTION_THROW_SPECIAL, new KeyMapping(Input.Keys.K));
        keyMap.put(InputAction.ACTION_SWITCH_SPECIAL, new KeyMapping(Input.Keys.L));
    }

    public boolean isDown(InputAction key){
        return keyMap.get(key).isDown();
    }
    public boolean isPressed(InputAction key){
        return keyMap.get(key).isPressed();
    }

    public void actFrame(){
        for(KeyMapping keyMapping : keyMap.values()){
            keyMapping.actFrame();
        }
    }

    public void setKey(InputAction action, int keyCode){
        keyMap.put(action,new KeyMapping(keyCode));
    }
    public int getKeyCode(InputAction action){
        return keyMap.get(action).getKey();
    }

    public String getKeyName(InputAction action){
        if(isAssigned(action)) {
            return Input.Keys.toString(keyMap.get(action).getKey());
        }else{
            throw new RuntimeException("Action '"+action.getLabel()+"' is not assigned a key.");
        }
    }
    public boolean isAssigned(InputAction action){
        return keyMap.get(action) != null;
    }

    @Override
    public boolean keyDown(int keycode) {
        System.out.println("keycode = " + keycode);
        for(KeyMapping km : keyMap.values()){
            if(km.getKey()==keycode){
                km.press();
            }
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        for(KeyMapping km : keyMap.values()){
            if(km.getKey()==keycode){
                km.release();
            }
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}

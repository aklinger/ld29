package com.aa.ld29.dal;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * Loads and stores all the Textures.
 *
 * @author Toni
 */
public class TextureManager implements ResourceProvider {
    //Attributes
    private static HashMap<String, Texture> textures;
    private static TextureManager instance;

    //Constructors
    private TextureManager() {
    }

    static {
        textures = new HashMap<>();
    }

    public static TextureManager getInstance() {
        if (instance == null) {
            instance = new TextureManager();
        }
        return instance;
    }

    //Methods
    public void loadTextures(String file) throws IOException {
        FileHandle fileHandle = getExternalFileHandleIfPossible(file);
        BufferedReader reader = fileHandle.reader(256);
        String line;
        //VARIABLES
        String currentFolder = "";
        //
        while ((line = reader.readLine()) != null) {
            if (!line.isEmpty() && !line.startsWith("#")) {
                if (line.startsWith("$")) {
                    String[] split = line.substring(1).split(":");
                    if ("folder".equals(split[0])) {
                        if (split.length > 1) {
                            currentFolder = split[1];
                            if (!currentFolder.endsWith("/")) {
                                currentFolder = currentFolder + "/";
                            }
                        } else {
                            currentFolder = "";
                        }
                    }
                } else {
                    String[] split = line.split(":");
                    Texture.TextureFilter filter = Texture.TextureFilter.Nearest;
                    if (split.length == 3 && split[2].toLowerCase().startsWith("n")) {
                        filter = Texture.TextureFilter.Nearest;
                    } else if (split.length == 3 && split[2].toLowerCase().startsWith("l")) {
                        filter = Texture.TextureFilter.Linear;
                    }
                    loadTexture(currentFolder + split[0], split[1], filter);
                }
            }
        }
    }

    @Override
    public Texture getTexture(String name) {
        return textures.get(name);
    }

    public static void loadTexture(String file, String name, Texture.TextureFilter filter) {
        Texture texture = new Texture(getExternalFileHandleIfPossible(file));
        texture.setFilter(filter, filter);
        textures.put(name, texture);
    }

    public static void disposeTexture(String name) {
        getInstance().getTexture(name).dispose();
    }

    public static void disposeAllTextures() {
        for (Texture texture : textures.values()) {
            texture.dispose();
        }
    }

    public static FileHandle getExternalFileHandleIfPossible(String filename) {
        /*FileHandle fh = Gdx.files.absolute(LD29Game.pathOfJar+filename);
        if(!fh.exists()){*/
        FileHandle fh = Gdx.files.internal(filename);
        /*}*/
        return fh;
    }
}

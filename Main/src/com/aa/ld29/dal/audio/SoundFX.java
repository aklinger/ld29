package com.aa.ld29.dal.audio;

/**
 * Created by Toni on 28.04.2014.
 */
public class SoundFX {
    public static final String EXPLOSION_1 = "sfx/Explosion2.wav";
    public static final String EXPLOSION_2 = "sfx/Explosion10.wav";
    public static final String EXPLOSION_3 = "sfx/Explosion11.wav";
    //public static final String EXPLOSION6 = "sfx/Explosion12.wav";

    public static final String[] LIGHT_EXPLOSIONS = {EXPLOSION_2, EXPLOSION_3};
    public static final String[] HEAVY_EXPLOSIONS = {EXPLOSION_1};

    public static final String HURT_1 = "sfx/Hit_Hurt10.wav";
    public static final String HURT_2 = "sfx/Hit_Hurt11.wav";
    public static final String HURT_3 = "sfx/Hit_Hurt13.wav";
    public static final String HURT_4 = "sfx/Hit_Hurt15.wav";

    public static final String PICKUP = "sfx/Powerup.wav";

    public static final String THROW = "sfx/Throw.wav";

    public static final String SHOOT = "sfx/Laser_Shoot6.wav";
}

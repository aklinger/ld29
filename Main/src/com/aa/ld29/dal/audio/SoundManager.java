package com.aa.ld29.dal.audio;

import com.aa.ld29.log.MyLogger;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

import java.util.HashMap;
import java.util.Map;

/**
 * Manages a Cache of sound Effects.
 * <p/>
 * Created by Toni on 12.07.2014.
 */
public class SoundManager {
    //Attributes
    private float volume = 1f;
    private boolean enabled = true;
    private final Map<String, Sound> sounds;

    //Constructors
    private static SoundManager instance;

    public static SoundManager getInstance() {
        if (instance == null) {
            instance = new SoundManager();
        }
        return instance;
    }

    public SoundManager() {
        sounds = new HashMap<>();
    }

    //Methods
    public void playRandom(String... sounds) {
        int chosen = (int) (Math.random() * sounds.length);
        play(sounds[chosen]);
    }

    public void play(String sound) {
        if (!enabled) {
            return;
        }

        Sound soundToPlay = sounds.get(sound);
        if (soundToPlay == null) {
            FileHandle soundFile = Gdx.files.internal(sound);
            soundToPlay = Gdx.audio.newSound(soundFile);
            sounds.put(sound, soundToPlay);
        }

        //MyLogger.debug("Playing sound: " + sound);

        soundToPlay.play(volume);
    }

    public void dispose() {
        MyLogger.log("Disposing the sound manager");
        for (Sound sound : sounds.values()) {
            sound.stop();
            sound.dispose();
        }
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public float getVolume() {
        return volume;
    }

    public boolean isEnabled() {
        return enabled;
    }
}

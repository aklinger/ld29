package com.aa.ld29.dal;

import com.aa.ld29.dal.audio.MusicManager;
import com.aa.ld29.dal.audio.SoundManager;
import com.aa.ld29.dal.input.ControlSettings;
import com.aa.ld29.dal.input.InputAction;

/**
 * Created by Toni on 11.07.2014.
 */
public class BelowEarthPreferences extends OptionsPreferences {

    private static final String PREFERENCES_KEY = "belowEarth";

    private static final String SOUND_VOLUME = "sfx.volume";
    private static final String SOUND_MUTE = "sfx.mute";
    private static final String MUSIC_VOLUME = "music.volume";
    private static final String MUSIC_MUTE = "music.mute";

    private static BelowEarthPreferences instance;

    public static BelowEarthPreferences getInstance() {
        if (instance == null) {
            instance = new BelowEarthPreferences();
        }
        return instance;
    }

    private BelowEarthPreferences() {
        super(PREFERENCES_KEY);
    }

    public void loadSettings() {
        loadSoundSettings();
        loadControlSettings();
    }

    private void loadSoundSettings() {
        SoundManager.getInstance().setVolume(getFloat(SOUND_VOLUME, 0.8f));
        SoundManager.getInstance().setEnabled(!getBoolean(SOUND_MUTE, false));
        MusicManager.getInstance().setVolume(getFloat(MUSIC_VOLUME, 0.6f));
        MusicManager.getInstance().setEnabled(!getBoolean(MUSIC_MUTE, false));
    }

    private void loadControlSettings(){
        for(InputAction ia : InputAction.values()){
            int keycode = getInteger(ia.getCode(),-1);
            if(keycode != -1){
                ControlSettings.getInstance().setKey(ia,keycode);
            }
        }
    }

    public void saveSettings() {
        saveSoundSettings();
        saveControlSettings();
    }

    private void saveSoundSettings() {
        setFloat(SOUND_VOLUME, SoundManager.getInstance().getVolume());
        setBoolean(SOUND_MUTE, !SoundManager.getInstance().isEnabled());
        setFloat(MUSIC_VOLUME, MusicManager.getInstance().getVolume());
        setBoolean(MUSIC_MUTE, !MusicManager.getInstance().isEnabled());
    }

    private void saveControlSettings(){
        for(InputAction ia : InputAction.values()){
            setInteger(ia.getCode(),ControlSettings.getInstance().getKeyCode(ia));
        }
    }
}

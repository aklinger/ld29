package com.aa.ld29.logic.levels;

import com.aa.ld29.dal.input.ControlSettings;
import com.aa.ld29.dal.input.InputAction;
import com.aa.ld29.dal.level.LevelDal;
import com.aa.ld29.dal.level.SimpleLevelLoader;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.tile.Tile;
import com.aa.ld29.logic.tile.TileManager;
import com.aa.ld29.maths.Calc;
import com.badlogic.gdx.Gdx;

import java.io.IOException;

/**
 * Created by Toni on 28.04.2014.
 */
public class LevelLibrary {

    public static final int tileSize = 32;

    private static final int TUTORIAL = 1;
    private static final int DIGDRI_INTRO = 2;
    private static final int DIGDRI_WALL = 3;
    private static final int WALL_DESTRUCTION_INTRO = 4;
    private static final int DIGDRI_RANDOM = 5;
    private static final int TENTACLE_PATH = 6;
    private static final int TENTACLE_FIELD = 7;
    private static final int SHARK_WITH_AMMO = 8;
    private static final int BOSS_DIGDRI = 9;
    private static final int THIEF_INTRO = 10;
    private static final int THIEF_RANDOM = 11;
    private static final int SHARK_RANDOM = 12;
    private static final int BOSS_SHARK = 13;
    private static final int LABYRINTH = 14;
    public static final int BOSS_TENTACLE = 15;
    public static final int SHARK_INTRO = 16;
    public static final int SHARK_BASE = 17;
    public static final int INTRO_BOMBS = 18;
    public static final int THIEF_TEST1 = 19;
    public static final int PING_PONG = 20;
    public static final int DUNGEON = 21;
    public static final int NINJA = 22;
    public static final int SHARKY = 23;
    public static final int HARDCORE = 24;
    public static final int RANDOMIZED = -1;

    private static int[] levels = {TUTORIAL, INTRO_BOMBS, DIGDRI_INTRO,DIGDRI_WALL,WALL_DESTRUCTION_INTRO,DIGDRI_RANDOM,TENTACLE_PATH,TENTACLE_FIELD, SHARK_INTRO,SHARK_BASE,THIEF_INTRO, THIEF_TEST1,BOSS_DIGDRI,SHARKY,PING_PONG, NINJA, SHARK_WITH_AMMO, THIEF_RANDOM,SHARK_RANDOM,DUNGEON,RANDOMIZED,HARDCORE,LABYRINTH,RANDOMIZED,BOSS_SHARK};

    public static boolean allLevelsFinished(int level){
        return level > levels.length;
    }

    public static EntityManager createLevel(int level, int gameWidth, int gameHeight, boolean multiplayer){
        LevelDal dal = new SimpleLevelLoader(multiplayer);

        EntityManager manager = new EntityManager(gameWidth,gameHeight,multiplayer);
        TileManager tileManager;
        int levelWidth, levelHeight;

        if(level > levels.length){
            level = levels.length - 1;
        }

        int chosenLevel = levels[level-1];

        if(chosenLevel == TUTORIAL){
            //Empty Level
            levelWidth = levelHeight = 10;
            tileManager = TileManager.generateRandomMap(0,levelWidth,levelHeight,0,tileSize);
            tileManager.createMapBorder(1, 2, Tile.WALL);
            manager.setTileManager(tileManager);
            ControlSettings controls = ControlSettings.getInstance();
            manager.setHelpText("Move Around with "+ controls.getKeyName(InputAction.ACTION_MOVE_UP)+", "+
                    controls.getKeyName(InputAction.ACTION_MOVE_DOWN)+", "+
                    controls.getKeyName(InputAction.ACTION_MOVE_LEFT)+" and "+
                    controls.getKeyName(InputAction.ACTION_MOVE_RIGHT)+".");

            manager.createPlayer(2,levelHeight/2, true);
            manager.createExit(levelWidth - 3, levelHeight / 2);
        }else if(chosenLevel == DIGDRI_INTRO){
            //First enemy contact
            levelWidth = levelHeight = 12;
            tileManager = TileManager.generateRandomMap(0,levelWidth,levelHeight,0,tileSize);
            tileManager.createMapBorder(1,2, Tile.WALL);
            manager.setTileManager(tileManager);
            manager.setHelpText("Throw Bombs at enemies to kill them. They don't take any damage while underground.");

            manager.createPlayer(2, levelHeight / 2, true);
            manager.createExit(levelWidth - 3, levelHeight / 2);
            manager.createEnemy(levelWidth/2,levelHeight/2,2);
        }else if(chosenLevel == DIGDRI_WALL){
            //Wall of digdris
            levelWidth = levelHeight = 12;
            tileManager = TileManager.generateRandomMap(0,levelWidth,levelHeight,0,tileSize);
            int randy = 2;
            tileManager.createMapBorder(1,randy, Tile.WALL);
            manager.setTileManager(tileManager);
            manager.setHelpText("Pick up healthpacks to regain health. You also gain some health after completing a level.");

            manager.createPlayer(2, levelHeight / 2, true);
            manager.createExit(levelWidth - 3, levelHeight / 2);
            for (int i = randy; i < levelHeight-randy; i += 2) {
                int displacementX = (int)(Math.random()*3)-1;
                manager.createEnemy(levelWidth/2+displacementX,i,2);
            }
            manager.createGoodie(levelWidth - 4, levelHeight / 2 - 2, 2 ,false);
        }else if(chosenLevel == WALL_DESTRUCTION_INTRO){
            //You have to bomb you way through this wall
            levelWidth = 12;
            levelHeight = 10;
            tileManager = new TileManager(levelWidth,levelHeight,tileSize);
            tileManager.fillMapWith(Tile.FLOOR);
            tileManager.createMapBorder(1, 1, Tile.WALL);
            manager.setTileManager(tileManager);
            manager.setHelpText("You can use special bombs with "+ControlSettings.getInstance().getKeyName(InputAction.ACTION_THROW_SPECIAL)+" and switch through them with "+ControlSettings.getInstance().getKeyName(InputAction.ACTION_SWITCH_SPECIAL)+".");

            manager.createPlayer(2, levelHeight / 2, true);
            manager.createGoodie(3,3,3,false);
            manager.createGoodie(3,levelHeight-3,4,false);
            manager.createExit(levelWidth - 3, levelHeight / 2);
            tileManager.createWall(levelWidth/2-1,0,levelWidth/2+1,levelHeight,Tile.WALL);
        }else if(chosenLevel == DIGDRI_RANDOM){
            //Random digdris
            levelWidth = 18;
            levelHeight = 12;
            tileManager = TileManager.generateRandomMap(0,levelWidth,levelHeight,0,tileSize);
            tileManager.createMapBorder(10, 10, Tile.WALL);
            manager.setTileManager(tileManager);
            manager.setHelpText("Some levels are partially randomly generated, don't worry if you don't succeed at first.");

            manager.createPlayer(2, levelHeight / 2, true);
            manager.createExit(levelWidth - 3, levelHeight / 2);
            for (int i = 0; i < 10; i++) {
                int xpos = (int)(Math.random()*levelWidth);
                int ypos = (int)(Math.random()*levelHeight);
                tileManager.createMapHole(xpos,ypos,5,false,Tile.FLOOR);
                if(i % 2 == 0) {
                    manager.createEnemy(xpos, ypos, 2);
                }
            }
        }else if(chosenLevel == TENTACLE_PATH){
            //Path with tentacles //easy
            levelWidth = levelHeight = 16;
            tileManager = TileManager.generateRandomMap(0,levelWidth,levelHeight,0,tileSize);
            tileManager.createMapBorder(10, 10, Tile.WALL);
            manager.setTileManager(tileManager);
            manager.setHelpText("You can hold down the "+ControlSettings.getInstance().getKeyName(InputAction.ACTION_THROW_BOMB)+" key to 'cook' your bombs.");

            int startx = 2;
            int starty = (int)(Math.random()*levelHeight/2);

            int posx = startx;
            int posy = starty;
            int direction = -1;
            int n = 0;

            while ( (posx < levelWidth - 2 || posy < levelHeight - 2) && n < 100) {
                n++;
                tileManager.createMapHole(posx,posy,2,false,Tile.FLOOR);
                int random = (int)(Math.random()*100);
                if(random < 50 && direction != 1){
                    posx++;
                    direction = 0;
                }else if(random < 60 && direction != 0){
                    posx--;
                    direction = 1;
                }else if(random < 90 && direction != 3){
                    posy++;
                    direction = 2;
                }else if(random < 100 && direction != 2){
                    posy--;
                    direction = 3;
                }
                posx = (int) Calc.fastRange(posx, 0, levelWidth - 1);
                posy = (int) Calc.fastRange(posy,0, levelHeight-1);
                if(n%8 == 0 && posx != startx && posy != starty){
                    //tileManager.createMapHole(posx,posy,5,Tile.FLOOR);
                    manager.createEnemy(posx,posy,1);
                }
                if(n%37 == 0 && !(posx == startx && posy == starty)){
                    //tileManager.createMapHole(posx,posy,5,Tile.FLOOR);
                    manager.createGoodie(posx, posy, 2, false);
                }
            }

            manager.createPlayer(startx, starty, true);
            manager.createExit(posx, posy);
        }else if(chosenLevel == TENTACLE_FIELD){
            //Field of Tentacles //not cool enough
            levelWidth = 16;
            levelHeight = 12;
            int rand = 2;
            tileManager = TileManager.generateRandomMap(0,levelWidth,levelHeight,0,tileSize);
            tileManager.createMapBorder(1, rand, Tile.WALL);
            manager.setTileManager(tileManager);

            manager.createPlayer(2, levelHeight / 2, true);
            manager.createExit(levelWidth - 3, levelHeight / 2);
            for (int i = levelHeight-rand-1; i >= rand; i--) {
                int displacementX = (int)(Math.random()*3)-1;
                //int displacementY = (int)(Math.random()*(levelHeight-rand*2))-rand;
                manager.createEnemy(levelWidth/2-1+displacementX,i,1);

                displacementX = (int)(Math.random()*3)-1;
                manager.createEnemy((levelWidth/2)+2+displacementX,i,1);
            }
        }else if(chosenLevel == SHARK_WITH_AMMO){
            //MOTHERFUCKING SHARKS
            levelWidth = levelHeight = 16;
            tileManager= new TileManager(levelWidth,levelHeight,tileSize);
            tileManager.fillMapWith(Tile.WALL);
            manager.setTileManager(tileManager);

            manager.createPlayer(2, levelHeight / 2, true);
            tileManager.createMapHole(2, levelHeight / 2, 5, true, Tile.FLOOR);
            manager.createExit(levelWidth - 3, levelHeight / 2);

            int rand = 3;
            for (int i = 0; i < 10; i++) {
                int xpos = (int)(Math.random()*(levelWidth-rand*2))+rand;
                int ypos = (int)(Math.random()*(levelHeight-rand*2))+rand;
                tileManager.createMapHole(xpos,ypos,3,true,Tile.FLOOR);
                //manager.createEnemy(xpos,ypos,2);
            }
            for (int i = 0; i < 4; i++) {
                int xpos = (int)(Math.random()*(levelWidth-rand*2))+rand;
                int ypos = (int)(Math.random()*(levelHeight-rand*2))+rand;
                tileManager.createMapHole(xpos,ypos,10,true,Tile.FLOOR);
                manager.createEnemy(xpos,ypos,3);
            }
            int xpos = (int)(Math.random()*(levelWidth-rand*2))+rand;
            int ypos = (int)(Math.random()*(levelHeight-rand*2))+rand;
            manager.createGoodie(xpos, ypos, 4, false);
        }else if(chosenLevel == BOSS_DIGDRI){
            //BOSSFIGHT DIGDRI
            levelWidth = levelHeight = 20;
            tileManager= new TileManager(levelWidth,levelHeight,tileSize);
            tileManager.fillMapWith(Tile.FLOOR);
            manager.setTileManager(tileManager);

            manager.createPlayer(2, levelHeight / 2, true);
            tileManager.createMapHole(2, levelHeight / 2, 5, true, Tile.FLOOR);
            manager.createGoodie(3, levelHeight / 2 - 2, 2, false);
            manager.createGoodie(3, levelHeight / 2 + 2, 3, false);

            manager.createExit(levelWidth - 3, levelHeight / 2);
            manager.createGoodie(levelWidth - 4, levelHeight / 2 - 2, 3, true);
            manager.createGoodie(levelWidth - 4, levelHeight / 2 + 2, 2, true);

            manager.createEnemy(levelWidth /2,levelHeight/2,2,true);
        }else if(chosenLevel == THIEF_INTRO){
            //Introducing the Thief
            levelWidth = 18;
            levelHeight = 10;
            tileManager = new TileManager(levelWidth,levelHeight,tileSize);
            tileManager.fillMapWith(Tile.FLOOR);
            tileManager.createMapBorder(1, 1, Tile.WALL);
            manager.setTileManager(tileManager);

            manager.createPlayer(2, levelHeight / 2, true);
            manager.createExit(levelWidth - 3, levelHeight / 2);

            tileManager.createWall(levelWidth/2,0,levelWidth/2+1,levelHeight,Tile.WALL);
            manager.createEnemy(levelWidth/2-1,levelHeight/2,4,false);

            tileManager.createWall((int)(levelWidth*(3f/4f)),0,(int)(levelWidth*(3f/4f)+1),levelHeight,Tile.WALL);
            manager.createEnemy((int)(levelWidth*(3f/4f)-1),levelHeight/2-2,4,false);
            manager.createEnemy((int)(levelWidth*(3f/4f)-1),levelHeight/2+2,4,false);
        }else if(chosenLevel == THIEF_RANDOM){
            //Random Thiefs
            levelWidth = 24;
            levelHeight = 12;
            tileManager = TileManager.generateRandomMap(0,levelWidth,levelHeight,0,tileSize);
            tileManager.createMapBorder(10, 10, Tile.WALL);
            manager.setTileManager(tileManager);

            manager.createPlayer(2, levelHeight / 2, true);
            tileManager.createMapHole(2, levelHeight / 2, 3, true, Tile.FLOOR);
            manager.createExit(levelWidth - 3, levelHeight / 2);
            for (int i = 0; i < 8; i++) {
                int lexl = 3;
                int xpos = (int)(Math.random()*levelWidth-lexl)+lexl/2;
                int ypos = (int)(Math.random()*levelHeight-lexl)+lexl/2;
                tileManager.createMapHole(xpos,ypos,5,false,Tile.FLOOR);
                if(Math.random()<0.5) {
                    manager.createEnemy(xpos, ypos, 4);
                }else if(Math.random()<0.5) {
                    manager.createEnemy(xpos + 1, ypos, 2);
                }else{
                    manager.createEnemy(xpos - 1, ypos, 1);
                }
            }
        }else if(chosenLevel == SHARK_RANDOM){
            //random MOTHERFUCKING SHARKS with digdris //not balanced
            levelWidth = levelHeight = 20;
            tileManager= new TileManager(levelWidth,levelHeight,tileSize);
            tileManager.fillMapWith(Tile.WALL);
            manager.setTileManager(tileManager);

            manager.createPlayer(2, levelHeight / 2, true);
            tileManager.createMapHole(2, levelHeight / 2, 5, true, Tile.FLOOR);
            manager.createExit(levelWidth - 3, levelHeight / 2);
            for (int i = 0; i < 10; i++) {
                int xpos = (int)(Math.random()*levelWidth);
                int ypos = (int)(Math.random()*levelHeight);
                tileManager.createMapHole(xpos,ypos,5,true,Tile.FLOOR);
                manager.createEnemy(xpos,ypos,2);
            }
            for (int i = 0; i < 5; i++) {
                int xpos = (int)(Math.random()*levelWidth);
                int ypos = (int)(Math.random()*levelHeight);
                tileManager.createMapHole(xpos,ypos,10,true,Tile.FLOOR);
                manager.createEnemy(xpos,ypos,3);
            }
        }else if(chosenLevel == LABYRINTH){
            //A strange labyrinth
            levelWidth = levelHeight = 25;
            tileManager= new TileManager(levelWidth,levelHeight,tileSize);
            manager.setTileManager(tileManager);
            tileManager.fillMapWith(Tile.WALL);

            int chamberFloorSize = 3;
            int wallStrength = 1;
            int chamberSize = chamberFloorSize+wallStrength*2;
            int chamberCountX = levelWidth/(chamberSize);
            int chamberCountY = levelHeight/chamberSize;


            for (int i = 0; i < chamberCountX; i++) {
                for (int j = 0; j < chamberCountY; j++) {
                    int xpos = i*chamberSize;
                    int ypos = j*chamberSize;
                    tileManager.createWall(xpos+wallStrength,ypos+wallStrength,xpos+chamberFloorSize,ypos+chamberFloorSize,Tile.FLOOR);
                    int rand;
                    rand = (int)(Math.random()*100);
                    if(rand < 25 && i > 0){//LEFT
                        tileManager.createWall(xpos-wallStrength*2+1,ypos+1,xpos,ypos+chamberFloorSize,Tile.FLOOR);
                    }
                    rand = (int)(Math.random()*100);
                    if(rand < 25 && j > 0){//DOWN
                        tileManager.createWall(xpos+1,ypos-wallStrength*2+1,xpos+chamberFloorSize,ypos,Tile.FLOOR);
                    }
                    rand = (int)(Math.random()*100);
                    if(rand < 55 && i < chamberCountX - 1){//RIGHT
                        tileManager.createWall(xpos+chamberFloorSize+1,ypos+wallStrength,xpos+chamberFloorSize+wallStrength*2,ypos+chamberFloorSize,Tile.FLOOR);
                    }
                    rand = (int)(Math.random()*100);
                    if(rand < 50 && j < chamberCountY - 1) {//UP
                        tileManager.createWall(xpos+wallStrength, ypos + chamberFloorSize + 1, xpos + chamberFloorSize, ypos + chamberFloorSize + wallStrength * 2, Tile.FLOOR);
                    }

                    int centerx = xpos +chamberSize/2;
                    int centery = ypos +chamberSize/2;
                    if(i == 0 && j == 0){
                        manager.createPlayer(centerx,centery, true);
                    }else if(i == chamberCountX -1 && j == chamberCountY-1){
                        manager.createExit(centerx, centery);
                    }else if(i == 0 && j == chamberCountY-1){
                        manager.createGoodie(centerx, centery, 2, false);
                    }else if(i == chamberCountX -1 && j == 0){
                        manager.createGoodie(centerx, centery, 3, false);
                    }else{
                        manager.createEnemy(centerx,centery,(int)(Math.random()*4)+1);
                    }
                }
            }
        }else if(chosenLevel == BOSS_SHARK){
            //BOSSFIGHT SHARK
            levelWidth = levelHeight = 22;
            tileManager= new TileManager(levelWidth,levelHeight,tileSize);
            tileManager.fillMapWith(Tile.FLOOR);
            tileManager.createMapBorder(1,1, Tile.WALL);
            manager.setTileManager(tileManager);

            manager.createPlayer(2, levelHeight / 2, true);
            manager.createGoodie(3, levelHeight / 2 - 2, 2, false);
            manager.createGoodie(3, levelHeight / 2 + 2, 3, false);

            manager.createExit(levelWidth - 3, levelHeight / 2);
            manager.createGoodie(levelWidth - 4, levelHeight / 2 - 2, 3, true);
            manager.createGoodie(levelWidth - 4, levelHeight / 2 + 2, 2, true);

            manager.createEnemy(levelWidth /2,levelHeight/2,3,true);
        }else if(chosenLevel == BOSS_TENTACLE){
            //BOSSFIGHT cthulhu
            levelWidth = levelHeight = 22;
            tileManager= new TileManager(levelWidth,levelHeight,tileSize);
            tileManager.fillMapWith(Tile.FLOOR);
            tileManager.createMapBorder(1,1, Tile.WALL);
            manager.setTileManager(tileManager);

            manager.createPlayer(2, levelHeight / 2, true);
            manager.createGoodie(3, levelHeight / 2 - 2, 2, false);
            manager.createGoodie(3, levelHeight / 2 + 2, 3, false);

            manager.createExit(levelWidth - 3, levelHeight / 2);
            manager.createGoodie(levelWidth - 4, levelHeight / 2 - 2, 3, true);
            manager.createGoodie(levelWidth - 4, levelHeight / 2 + 2, 2, true);

            manager.createEnemy(levelWidth /2,levelHeight/2,1,true);
        }else if(chosenLevel == SHARK_INTRO){
            //First time shark
            levelWidth = 15;
            levelHeight = 12;
            tileManager = TileManager.generateRandomMap(0,levelWidth,levelHeight,0,tileSize);
            tileManager.createMapBorder(1,2, Tile.WALL);
            manager.setTileManager(tileManager);

            manager.createPlayer(2, levelHeight / 2, true);
            manager.createExit(levelWidth - 3, levelHeight / 2);
            manager.createEnemy(levelWidth/2+2,5,3);
        }else if(chosenLevel == SHARK_BASE){
            //Stocking up on health and ammo
            levelWidth = levelHeight = 20;
            tileManager = new TileManager(levelWidth,levelHeight,tileSize);
            tileManager.fillMapWith(Tile.WALL);
            manager.setTileManager(tileManager);

            int startx = 4;
            int starty = 4;

            int posx = startx;
            int posy = starty;
            int n = 0;

            while ( (posx < levelWidth - 2 || posy < levelHeight - 2) && n < 100) {
                n++;
                tileManager.createMapHole(posx,posy,3,false,Tile.FLOOR);
                int random = (int)(Math.random()*100);
                if(random < 25){
                    posx++;
                }else if(random < 50){
                    posy++;
                }else if(random < 75){
                    posx+=2;
                }else{
                    posy+=2;
                }
                posx = (int)Calc.fastRange(posx,0, levelWidth-1);
                posy = (int)Calc.fastRange(posy,0, levelHeight-1);
                if(n%15 == 0 && posx != startx && posy != starty){
                    //tileManager.createMapHole(posx,posy,5,Tile.FLOOR);
                    manager.createEnemy(posx,posy,3);
                }
            }

            manager.createPlayer(startx, starty, true);
            manager.createExit(posx, posy);

            posx = (int)(Math.random()*levelWidth/2)+levelWidth/4;
            posy = (int)(Math.random()*levelHeight/2)+levelHeight/4;
            tileManager.createMapHole(posx,posy,3,false,Tile.FLOOR);
            manager.createGoodie(posx,posy,2,false);
            manager.createEnemy(posx-1,posy,2);
            manager.createEnemy(posx+1,posy,1);

            posx = (int)(Math.random()*levelWidth/2)+levelWidth/4;
            posy = (int)(Math.random()*levelHeight/2)+levelHeight/4;
            tileManager.createMapHole(posx,posy,3,false,Tile.FLOOR);
            manager.createGoodie(posx,posy,3,false);
            manager.createEnemy(posx-1,posy,2);
            manager.createEnemy(posx+1,posy,3);
        }else if(chosenLevel == INTRO_BOMBS) {
            try {
                manager = dal.loadFromFile(Gdx.files.internal("levels/testLevel1.lvl"));
                manager.setHelpText("Press " + ControlSettings.getInstance().getKeyName(InputAction.ACTION_THROW_BOMB) + " to throw bombs. You throw them further when moving at full speed");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(chosenLevel == THIEF_TEST1) {
            try {
                manager = dal.loadFromFile(Gdx.files.internal("levels/thiefLevel.lvl"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(chosenLevel == PING_PONG) {
            try {
                manager = dal.loadFromFile(Gdx.files.internal("levels/pingPong.lvl"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(chosenLevel == DUNGEON) {
            try {
                manager = dal.loadFromFile(Gdx.files.internal("levels/dungeon.lvl"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(chosenLevel == SHARKY) {
            try {
                manager = dal.loadFromFile(Gdx.files.internal( "levels/sharky.lvl" ));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(chosenLevel == NINJA) {
            try {
                manager = dal.loadFromFile(Gdx.files.internal("levels/ninja.lvl"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(chosenLevel == HARDCORE) {
            try {
                manager = dal.loadFromFile(Gdx.files.internal("levels/hardcore.lvl"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            manager.createRandomMap(level);
        }
        manager.setScreenSize(gameWidth,gameHeight);
        return manager;
    }
}

package com.aa.ld29.logic.tile;

import com.aa.ld29.dal.TextureManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 26.04.2014.
 */
public class Tile {
    public boolean solid;
    public boolean ore;
    public boolean lava;
    public boolean indestructible;

    private String textureName;

    private int tiles;

    public static final Tile FLOOR = new Tile("tile_floor", false, false, false);
    public static final Tile WALL = new Tile("tile_rock", 16, true, false, false, false);
    public static final Tile ORE = new Tile("tile_ore", true, true, false);
    public static final Tile LAVA_WALL = new Tile("tile_lavarock", true, false, true);
    public static final Tile LAVA = new Tile("tile_lava", false, false, true);
    public static final Tile BEDROCK = new Tile("tile_bedrock", 16, true, false, false, true);

    private Tile(String textureName, boolean solid, boolean ore, boolean lava) {
        this(textureName,1,solid,ore,lava,false);
    }

    private Tile(String textureName, int tiles, boolean solid, boolean ore, boolean lava, boolean indestructible) {
        this.textureName = textureName;
        this.solid = solid;
        this.ore = ore;
        this.lava = lava;
        this.indestructible = indestructible;
        this.tiles = tiles;
    }

    private int findCorrectFrame(TileManager tileManager, Vector2 position){
        //boolean up = false, down = false, left = false, right = false;
        boolean right = tileManager.getTile((int)(position.x+1),(int)(position.y)).solid;
        boolean left = tileManager.getTile((int)(position.x-1),(int)(position.y)).solid;
        boolean up = tileManager.getTile((int)(position.x),(int)(position.y+1)).solid;
        boolean down = tileManager.getTile((int)(position.x),(int)(position.y-1)).solid;
        //
        return (up ? 0 : 1) + (down ? 0 : 2) + (right ? 0 : 4) + (left ? 0 : 8);
    }

    public void render(SpriteBatch batch, TileManager tileManager, Vector2 position){
        TextureRegion textureRegion;
        Texture tex = TextureManager.getInstance().getTexture(textureName);
        if(tiles == 1) {
            textureRegion = TextureRegion.split(tex,tex.getWidth(),tex.getHeight())[0][0];
        }else{
            textureRegion = TextureRegion.split(tex,tex.getWidth()/tiles,tex.getHeight())[0][findCorrectFrame(tileManager, position)];
            FLOOR.render(batch,tileManager,position);
        }
        //Texture texture = TextureManager.getInstance().getTexture(t.getTextureName());
        batch.draw(textureRegion,(int)(position.x*tileManager.getTileSize()),(int)(position.y*tileManager.getTileSize()),tileManager.getTileSize(),tileManager.getTileSize());
    }

    public String getTextureName() {
        return textureName;
    }
}

package com.aa.ld29.logic.tile;

import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityFactory;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.objects.Bullet;
import com.aa.ld29.maths.Calc;
import com.aa.ld29.maths.Collision;
import com.aa.ld29.maths.CollisionUtil;
import com.aa.ld29.maths.HitBox;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 26.04.2014.
 */
public class TileManager {
    private Tile[][] map;
    private float tileSize;
    private Tile borderTile = Tile.BEDROCK;

    public TileManager(int width, int height, float tileSize){
        map = new Tile[width][height];
        this.tileSize = tileSize;
    }

    public void fillMapWith(Tile tileType){
        for (int i = 0; i < getMapWidth(); i++) {
            for (int j = 0; j < getMapHeight(); j++) {
                setTile(i,j,tileType);
            }
        }
    }

    public void createMapBorder(int borderSizeX, int borderSizeY, Tile tileType){
        for (int i = 0; i < getMapWidth(); i++) {
            for (int j = 0; j < getMapHeight(); j++) {
                if(i < borderSizeX || i > getMapWidth()-borderSizeX-1 || j < borderSizeY || j > getMapHeight()-borderSizeY-1){
                    setTile(i,j,tileType);
                }
            }
        }
    }

    public void createWall(int x1, int y1, int x2, int y2, Tile tileType){
        for (int i = x1; i <= x2; i++) {
            for (int j = y1; j <= y2; j++) {
                setTile(i, j, tileType);
            }
        }
    }

    public void createMapHole(int x, int y, int holesize, boolean raute, Tile tileType){
        for (int i = -holesize/2; i <= holesize/2; i++) {
            for (int j = -holesize/2; j <= holesize/2; j++) {
                if(!raute || Math.abs(i)+Math.abs(j) <= holesize/2) {
                    setTile(x + i, y + j, tileType);
                }
            }
        }
    }

    public static TileManager generateRandomMap(int method, int width, int height, float density, float tileSize){
        TileManager tm = new TileManager(width,height,tileSize);
        //init empty map
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                tm.setTile(i,j,Tile.FLOOR);
            }
        }
        //gen type of map
        if(method == 1){
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    int random = (int)(Math.random()*3);
                    Tile tile = Tile.FLOOR;
                    if(random == 0){
                        tile = Tile.FLOOR;
                    }else if(random == 1){
                        tile = Tile.WALL;
                    }else{
                        tile = Tile.ORE;
                    }
                    tm.setTile(i,j,tile);
                }
            }
        }else if(method == 2){
            for (int i = 0; i < width; i++) {
                    for (int j = 0; j < height; j++) {
                        int random = (int)(Calc.square(Math.random())*3);
                        Tile tile = Tile.FLOOR;
                        if(random == 0){
                            tile = Tile.FLOOR;
                        }else if(random == 1){
                            tile = Tile.WALL;
                        }else{
                            tile = Tile.ORE;
                        }
                        tm.setTile(i,j,tile);
                    }
            }
        }else if(method == 3){
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    int random = (int)(Calc.square(Math.random())*100);
                    Tile tile = Tile.FLOOR;
                    if(density > 40){
                        density = 40;
                    }
                    if(random < (60-density)){
                        tile = Tile.FLOOR;
                    }else if(random < 96){
                        tile = Tile.WALL;
                    }else{
                        tile = Tile.BEDROCK;
                    }
                    tm.setTile(i,j,tile);
                }
            }
        }
        return tm;
    }

    //Render Method
    public void render(SpriteBatch batch, Vector2 translation, int gameWidth, int gameHeight){
        Vector2 pos1 = getIndexAt(translation.x-gameWidth/2,translation.y-gameHeight/2);
        Vector2 pos2 = getIndexAt(translation.x+gameWidth/2,translation.y+gameHeight/2);
        for (int i = (int)pos1.x; i <= (int)(pos2.x); i++) {
            for (int j = (int)pos1.y; j <= (int)pos2.y; j++) {
                Tile t = getTile(i,j);
                t.render(batch,this,new Vector2(i,j));
            }
        }
    }

    //GamePlay Methods
    public void destroyTilesAt(float x, float y, float radius, EntityManager man) {
        Vector2 pos1 = getIndexAt(x-radius,y-radius);
        Vector2 pos2 = getIndexAt(x+radius,y+radius);
        for (int i = (int)pos1.x; i <= (int)(pos2.x); i++) {
            for (int j = (int)pos1.y; j <= (int)pos2.y; j++) {
                Tile t = getTile(i,j);
                Vector2 tilePos = getPositionAtIndex(i,  j);
                if(t.solid && !t.indestructible && tilePos.dst(x,y) < radius) {
                    setTile(i, j, Tile.FLOOR);
                    if (t.ore) {
                        Entity good = EntityFactory.createGoodie(tilePos.x, tilePos.y, 1);
                        man.spawnEntity(good);
                    }
                    if (t.solid && rangeCheck( i,  j)) {
                        Vector2 pos = getPositionAtIndex( i,  j);
                        EntityFactory.createParticles(pos.x, pos.y, tileSize, man);
                    }
                }
            }
        }
    }
    public void checkCollision(Entity entity,EntityManager man){
        Rectangle box = entity.getBoundingBox();
        Vector2 entPos1 = getIndexAt(box.getX(),box.getY());
        Vector2 entPos2 = getIndexAt(box.getX()+box.getWidth(),box.getY()+box.getHeight());
        Entity dummy = new Dummy();
        dummy.setFixated(true);
        dummy.setSolid(true);
        for (int i = (int)entPos1.x; i <= (int)(entPos2.x); i++) {
            for (int j = (int)entPos1.y; j <= (int)entPos2.y; j++) {
                Tile t = getTile(i,j);
                if(t.solid) {
                    if(getTile(i,j+1).solid){
                        dummy.setHitBox(new Rectangle(i * tileSize, j * tileSize, tileSize, tileSize*2));
                    }else if(getTile(i+1,j).solid){
                        dummy.setHitBox(new Rectangle(i * tileSize, j * tileSize, tileSize*2, tileSize));
                    }else {
                        dummy.setHitBox(new Rectangle(i * tileSize, j * tileSize, tileSize, tileSize));
                    }
                    Collision col = CollisionUtil.calcCollisionHitBoxes(entity, dummy, true, false);
                    if(col != null){
                        Entity.resolveCollision(col, !entity.isSpawnProtection());
                    }
                    if(entity instanceof Bullet) {
                        entity.interactWith(dummy,col,man); //Dirty bugfixes hooray
                    }
                }
            }
        }
    }

    private class Dummy extends Entity{
        @Override
        public void act(EntityManager man) {

        }

        @Override
        public int getType() {
            return HitBox.AABB;
        }
    }

    //Management Methods
    public Vector2 getPositionAtIndex(int x, int y){
        return new Vector2(x*tileSize+tileSize/2,y*tileSize+tileSize/2);
    }

    public Tile getTileAtPosition(float x, float y){
        return getTile((int)Math.floor(x / tileSize),(int)Math.floor(y/tileSize));
    }

    public Vector2 getIndexAt(float x, float y){
        return new Vector2((int)Math.floor(x / tileSize),(int)Math.floor(y/tileSize));
    }

    public Tile getTile(int x, int y){
        if(rangeCheck(x,y)){
            return map[x][y];
        }else{
            return borderTile;
        }
    }

    public void setTile(int x, int y, Tile tile){
        if(rangeCheck(x,y)) {
            map[x][y] = tile;
        }
    }

    public boolean rangeCheck(int x, int y){
        return (x >= 0 && y >= 0 && x < getMapWidth() && y < getMapHeight());
    }

    public int getMapWidth(){
        return map.length;
    }
    public int getMapHeight(){
        return map[0].length;
    }

    public int getTotalWidth(){
        return (int)(getMapWidth()*tileSize);
    }
    public int getTotalHeight(){
        return (int)(getMapHeight()*tileSize);
    }

    public float getTileSize() {
        return tileSize;
    }
}

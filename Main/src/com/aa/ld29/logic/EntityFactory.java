package com.aa.ld29.logic;

import com.aa.ld29.dal.audio.SoundFX;
import com.aa.ld29.dal.TextureManager;
import com.aa.ld29.logic.objects.*;
import com.aa.ld29.logic.objects.bombs.*;
import com.aa.ld29.logic.objects.enemies.*;
import com.aa.ld29.logic.objects.enemies.bossfights.DigdriBoss;
import com.aa.ld29.logic.objects.enemies.bossfights.DigdriSetting;
import com.aa.ld29.logic.particle.AnimatedParticle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

import static com.badlogic.gdx.graphics.g2d.Animation.PlayMode;

/**
 * Created by Toni on 16.04.2014.
 */
public class EntityFactory {

    public static TextureRegion loadSingleFrame(String name){
        Texture texture = TextureManager.getInstance().getTexture(name);
        return TextureRegion.split(texture,texture.getWidth(),texture.getHeight())[0][0];
    }

    public static TextureRegion[] loadAnimationFrames(String name, int length){
        TextureRegion[] texs = new TextureRegion[length];
        for (int i = 1; i <= length; i++) {
            Texture texture = TextureManager.getInstance().getTexture(name+i);
            texs[i-1] = TextureRegion.split(texture,texture.getWidth(),texture.getHeight())[0][0];
        }
        return texs;
    }

    public static TextureRegion[] loadAnimationFromSpriteSheet(String name, int start, int end, int cols, int rows){
        TextureRegion[] texs = new TextureRegion[end-start+1];
        Texture texture = TextureManager.getInstance().getTexture(name);
        TextureRegion[][] split = TextureRegion.split(texture,texture.getWidth()/cols,texture.getHeight()/rows);

        for (int i = 0; i <= end-start; i++) {
            texs[i] = split[(start+i)/cols][(start+i)%cols];
        }
        return texs;
    }

    public static Entity createPlayer(float x, float y){
        Fanta f = new Fanta("standing");

        Animation anim = new Animation(50,loadAnimationFromSpriteSheet("player_standing", 0, 1, 5, 1));
        anim.setPlayMode(PlayMode.LOOP);
        f.putAnimation("standing",anim);

        anim = new Animation(10,loadAnimationFromSpriteSheet("player_standing",2,4,5,1));
        anim.setPlayMode(PlayMode.LOOP);
        f.putAnimation("running",anim);

        //anim = new Animation(4,loadAnimationFrames("running",5));
        //anim.setPlayMode(Animation.LOOP);
        //f.putAnimation("running",anim);

        Spelunker player = new Spelunker(f);
        int size = 32;
        int randx = 10;
        int randy = 10;
        player.setHitBox(new Rectangle(randx,0,size-randx*2,size-randy));

        player.setMaxHealth(37);
        //player.setHealth(10);
        player.setCenter(x,y);
        player.setFx(0.93f);
        player.setFy(0.93f);

        return player;
    }

    public static Door createDoor(float x, float y){
        Fanta f = new Fanta("door");
        Animation anim = new Animation(5, loadSingleFrame("door"));
        f.putAnimation("door", anim);

        Door door = new Door(f);
        int rand = 8;
        door.setHitBox(new Rectangle(rand, rand, 32 - rand * 2, 32 - rand * 2));
        door.setCenter(x,y);

        return door;
    }

    public static Enemy createEnemy(float x,float y, int type, boolean boss){
        Enemy enemy;
        Fanta f = new Fanta("standing");

        if(type == 1){
            Animation anim = new Animation(25, loadAnimationFromSpriteSheet("rubbish", 0, 1, 2, 1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("standing", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("tentacle", 0, 4, 10, 1));
            f.putAnimation("emerging", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("tentacle", 0, 4, 10, 1));
            anim.setPlayMode(PlayMode.REVERSED);
            f.putAnimation("digging", anim);

            anim = new Animation(25, loadAnimationFromSpriteSheet("tentacle",3,4,10,1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("normal", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("tentacle", 5, 9, 10, 1));
            f.putAnimation("attacking", anim);

            if(boss){
                enemy = new Tentacle(f);
                enemy.setPointsWorth(100);
                enemy.setMaxHealth(7);
                float spriteSize = 90;
                enemy.setDrawableSize(spriteSize, spriteSize, true);
                int rand = 20;
                enemy.setHitBox(new Rectangle(rand, rand, spriteSize - rand * 2, spriteSize - rand * 2));
                enemy.setCenter(x, y);
                Tentacle tent = (Tentacle)enemy;
                tent.setAggroRange(75);
                tent.setPunchRange(50);
                tent.setHitRange(55);
                tent.setHitDamage(3);
            }else {
                enemy = new Tentacle(f);
                enemy.setMaxHealth(2);
                int rand = 5;
                enemy.setHitBox(new Rectangle(rand, rand, 32 - rand * 2, 32 - rand * 2));
                enemy.setCenter(x, y);
            }
        }else if(type == 2){
            Animation anim = new Animation(25, loadAnimationFromSpriteSheet("rubbish", 0, 1, 2, 1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("standing", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("digdri", 0, 2, 5, 1));
            f.putAnimation("emerging", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("digdri", 0, 2, 5, 1));
            anim.setPlayMode(PlayMode.REVERSED);
            f.putAnimation("digging", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("digdri", 2, 4, 5, 1));
            f.putAnimation("shooting", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("digdri",4,4,5,1));
            f.putAnimation("normal", anim);

            if(boss){
                enemy = new DigdriBoss(f);
                enemy.setPointsWorth(100);
                enemy.setMaxHealth(7);
                float spriteSize = 90;
                enemy.setDrawableSize(spriteSize, spriteSize, true);
                int rand = 15;
                enemy.setHitBox(new Rectangle(rand, rand, spriteSize - rand * 2, spriteSize - rand * 2));
                enemy.setCenter(x, y);
                DigdriBoss bossEnemy = (DigdriBoss)enemy;
                DigdriSetting set = new DigdriSetting(2,1000,60*0.9f,0.01f);
                bossEnemy.addSetting(set);
                set = new DigdriSetting(3,1000,60*0.3f,1.3f);
                bossEnemy.addSetting(set);
            }else {
                enemy = new Digdri(f);
                enemy.setMaxHealth(1);
                int rand = 5;
                enemy.setHitBox(new Rectangle(rand, rand, 32 - rand * 2, 32 - rand * 2));
                enemy.setCenter(x, y);
            }

        }else if(type == 3){
            Animation anim = new Animation(25, loadAnimationFromSpriteSheet("rubbish",0,1,2,1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("standing", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("shark", 0, 2, 5, 1));
            f.putAnimation("emerging", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("shark",1,2,5,1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("chasing", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("shark",3,4,5,1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("biting", anim);

            if(boss){
                enemy = new Sharktopus(f);
                enemy.setPointsWorth(100);
                enemy.setMaxHealth(7);
                float spriteSize = 100;
                enemy.setDrawableSize(spriteSize, spriteSize, true);
                int rand = 18;
                enemy.setHitBox(new Rectangle(rand, rand, spriteSize - rand * 2, spriteSize - rand * 2));
                enemy.setCenter(x, y);
                Sharktopus shark = (Sharktopus) enemy;
                shark.setFx(0.94f);
                shark.setFy(0.94f);
                shark.setAggroRange(1000);
                shark.setBiteRange(100);
                shark.setChaseAcceleration1(0.22f);
                shark.setChaseAcceleration2(0.14f);
                shark.setChaseMaxSpeed(12f);
                shark.setCooldown(60*3f);
                shark.setCollisionDamage(3);
            }else {
                enemy = new Sharktopus(f);
                enemy.setMaxHealth(1);
                int rand = 3;
                enemy.setHitBox(new Rectangle(rand, rand, 32 - rand * 2, 32 - rand * 2));
                enemy.setCenter(x, y);
            }
        }else if(type == 4){
            Animation anim = new Animation(25, loadAnimationFromSpriteSheet("rubbish",0,1,2,1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("standing", anim);


            anim = new Animation(10, loadAnimationFromSpriteSheet("thief",0,1,7,1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("chasing", anim);

            anim = new Animation(10, loadAnimationFromSpriteSheet("thief",2,3,7,1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("carrying", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("thief",4,6,7,1));
            f.putAnimation("throwing", anim);

            enemy = new Thief(f);
            enemy.setMaxHealth(1);
            int rand = 6;
            int borderTop = 14;
            enemy.setHitBox(new Rectangle(rand,rand,32-rand*2,32-rand-borderTop));
            enemy.setCenter(x,y);
        }else if(type == 5) {
            Animation anim = new Animation(25, loadAnimationFromSpriteSheet("rubbish", 0, 1, 2, 1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("standing", anim);

            anim = new Animation(10, loadAnimationFromSpriteSheet("walking_bomb", 0, 1, 3, 1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("chasing", anim);

            anim = new Animation(10, loadAnimationFromSpriteSheet("walking_bomb", 2, 2, 3, 1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("jumping", anim);

            enemy = new WalkingBomb(f);
            enemy.setMaxHealth(1);
            int rand = 8;
            enemy.setHitBox(new Rectangle(rand, rand, 32 - rand * 2, 32 - rand * 2));
            enemy.setCenter(x, y);
        }else if(type == 6){
            Animation anim = new Animation(5, loadSingleFrame("monster"));
            f.putAnimation("standing", anim);

            enemy = new Enemy(f);
            enemy.setMaxHealth(2);
            int rand = 2;
            enemy.setHitBox(new Rectangle(rand,rand,32-rand*2,32-rand*2));
            enemy.setCenter(x,y);
        }else{
            throw new RuntimeException("Unknown Enemy type: "+type);
        }

        return enemy;
    }

    public static Bomb createBomb(float x, float y, Entity cause, int type){
        Fanta f = new Fanta("burning");
        Bomb bomb;

        if(type == 1) {
            Animation anim = new Animation(5, loadAnimationFromSpriteSheet("bomb_burning", 0, 1, 4, 1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("burning", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("bomb_burning",2,3,4,1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("aboutToExplode", anim);

            bomb = new Bomb(f, cause, 60*2.5f,50,1);
            int rand = 11;
            bomb.setHitBox(new Rectangle(rand,rand,32-rand*2,32-rand*2));

        }else if(type == 2){
            Animation anim = new Animation(5, loadAnimationFromSpriteSheet("dynamite_burning", 1, 2, 5, 1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("burning", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("dynamite_burning",3,4,5,1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("aboutToExplode", anim);

            bomb = new Bomb(f, cause, 60*4f,95,3);
            int rand = 6;
            bomb.setHitBox(new Rectangle(rand,rand,32-rand*2,32-rand*2));
            bomb.setExplosionSounds(SoundFX.HEAVY_EXPLOSIONS);
        }else if(type == 3){
            Animation anim = new Animation(5, loadAnimationFromSpriteSheet("cluster_burning", 0, 0, 1, 1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("burning", anim);

            //anim = new Animation(5, loadAnimationFromSpriteSheet("dynamite_burning",3,4,5,1));
            //anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("aboutToExplode", anim);

            bomb = new ClusterBomb(f, cause, 60*2f,55,1,6,0.1f,8f);
            int rand = 10;
            bomb.setHitBox(new Rectangle(rand,rand,32-rand*2,32-rand*2));
            bomb.setExplosionSounds(SoundFX.HEAVY_EXPLOSIONS);
        }else if(type == 4){
            Animation anim = new Animation(5, loadAnimationFromSpriteSheet("shrapnel", 0, 0, 1, 1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("burning", anim);

            //anim = new Animation(5, loadAnimationFromSpriteSheet("dynamite_burning",3,4,5,1));
            //anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("aboutToExplode", anim);

            bomb = new Bomb(f, cause, (float)(60*1f+Math.random()*10),50,1);
            int rand = 4;
            bomb.setHitBox(new Rectangle(rand,rand,16-rand*2,16-rand*2));
        }else if(type == 5){
            Animation anim = new Animation(5, loadAnimationFromSpriteSheet("schnirdlkrocha_burning", 1, 2, 5, 1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("burning", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("schnirdlkrocha_burning",3,4,5,1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("aboutToExplode", anim);

            bomb = new SchnirdlKrocha(f, cause, (60*2.5f),50,1,1,6,9,(60*0.3f));
            int rand = 2;
            bomb.setHitBox(new Rectangle(rand,rand,16-rand*2,16-rand*2));
        }else if(type == 6){
            f.setCurrentAnimation("held");
            Animation anim = new Animation(5, loadAnimationFromSpriteSheet("mine_bomb", 1, 1, 4, 1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("held", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("mine_bomb", 0, 1, 4, 1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("thrown", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("mine_bomb",2,2,4,1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("focus", anim);

            anim = new Animation(5, loadAnimationFromSpriteSheet("mine_bomb",2,3,4,1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("aboutToExplode", anim);

            bomb = new Mine(f, cause, 60*0.8f,50,1,35);
            int rand = 4;
            bomb.setHitBox(new Rectangle(rand,rand,16-rand*2,16-rand*2));
        }else if(type == 7){
            Animation anim = new Animation(5, loadAnimationFromSpriteSheet("sticky_bomb", 0, 0, 1, 1));
            anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("burning", anim);

            //anim = new Animation(5, loadAnimationFromSpriteSheet("dynamite_burning",3,4,5,1));
            //anim.setPlayMode(PlayMode.LOOP);
            f.putAnimation("aboutToExplode", anim);

            bomb = new Sticky(f, cause, 60*3f,50,1);
            int rand = 12;
            bomb.setHitBox(new Rectangle(rand,rand,32-rand*2,32-rand*2));
        }else{
            throw new RuntimeException("Unknown Bomb type: "+type);
        }

        bomb.setCenter(x,y);
        bomb.setSpeedZ(1);
        bomb.setFx(0.95f);
        bomb.setFy(0.95f);

        return bomb;
    }

    public static Bullet createBullet(float x, float y, int type, Entity father){
        Fanta f = new Fanta("bullet");
        Bullet bullet;

        if(type == 1) {
            Animation anim = new Animation(5, loadSingleFrame("spit"));
            f.putAnimation("bullet", anim);

            bullet = new Bullet(f,father);
            int rand = 12;
            bullet.setHitBox(new Rectangle(rand,rand,32-rand*2,32-rand*2));
            bullet.setDamage(1);
            bullet.setSpeed(1.5f);
            bullet.setDuration(60*2.5f);
        }else if(type == 2 || type == 3){
            Animation anim = new Animation(5, loadSingleFrame("spit"));
            f.putAnimation("bullet", anim);

            bullet = new Bullet(f,father);

            int spriteSize = 50;
            bullet.setDrawableSize(50,50,false);
            int rand = 12;
            bullet.setHitBox(new Rectangle(rand, rand, spriteSize - rand * 2, spriteSize - rand * 2));
            bullet.setDamage(2);
            if(type == 2) {
                bullet.setSpeed(6.9f);
                bullet.setDuration(60 * 2.5f);
            }else{
                bullet.setSpeed(2.9f);
                bullet.setDuration(60 * 3.5f);
            }
        }else{
            throw new RuntimeException("Unknown Bullet type: "+type);
        }

        bullet.setCenter(x, y);

        return bullet;
    }

    public static Goodie createGoodie(float x, float y, int type){
        Fanta f = new Fanta("existing");
        Goodie goodie;

        if(type == 1) {
            Animation anim = new Animation(5, loadSingleFrame("crystal"));
            f.putAnimation("existing", anim);

            goodie = new Goodie(f);
            int rand = 1;
            goodie.setHitBox(new Rectangle(rand, rand, 32 - rand * 2, 32 - rand * 2));
            goodie.setScore(10);

        }else if(type == 2) {
            Animation anim = new Animation(5, loadSingleFrame("healthpack"));
            f.putAnimation("existing", anim);

            goodie = new Goodie(f);
            int rand = 2;
            goodie.setHitBox(new Rectangle(rand, rand, 32 - rand * 2, 32 - rand * 2));
            goodie.setHealing(3);

        }else if(type == 3) {
            Animation anim = new Animation(5, loadSingleFrame("ammopack_dynamite"));
            f.putAnimation("existing", anim);

            goodie = new Goodie(f);
            int rand = 2;
            goodie.setHitBox(new Rectangle(rand, rand, 32 - rand * 2, 32 - rand * 2));
            goodie.setAmmo(5,0);

        }else if(type == 4) {
            Animation anim = new Animation(5, loadSingleFrame("ammopack_cluster"));
            f.putAnimation("existing", anim);

            goodie = new Goodie(f);
            int rand = 2;
            goodie.setHitBox(new Rectangle(rand, rand, 32 - rand * 2, 32 - rand * 2));
            goodie.setAmmo(5,1);

        }else{
            throw new RuntimeException("Unknown Goodie type: "+type);
        }

        goodie.setCenter(x, y);

        return goodie;
    }

    public static void createExplosionParticles(float x, float y, float radius, EntityManager manager){
        Fanta f = new Fanta("exploding");
        Animation anim = new Animation(5, loadAnimationFromSpriteSheet("explosion",0,5,6,1));
        f.putAnimation("exploding", anim);
        AnimatedParticle sp = new AnimatedParticle(f);
        sp.setDrawableSize(radius*(0.9f)*2,radius*(0.9f)*2,true);
        sp.setX(x-sp.getImgW()/2);
        sp.setY(y-sp.getImgH()/2);
        manager.getParticles().addParticle(sp);
        //
        for (int i = 0; i < 5; i++) {
            int frame = (int)(Math.random()*3);
            f = new Fanta("stuff");
            anim = new Animation(0, loadAnimationFromSpriteSheet("pebbles",frame,frame,3,1));
            f.putAnimation("stuff",anim);
            sp = new AnimatedParticle(f);
            sp.setSpeedZ(1+(float)(Math.random()*2));
            sp.setDuration(100+(float)(Math.random()*30));
            int wtf = 4;
            sp.setX(x+(float)(Math.random()*wtf)-wtf/2-4);
            sp.setY(y + (float) (Math.random() * wtf) - wtf / 2 - 4);
            sp.setFx(0.98f);
            sp.setFy(0.98f);
            float flySpeed = 3+(float)(Math.random()*3);
            float direction = (float)(Math.random()*Math.PI*2);
            sp.setSx((float)(Math.sin(direction))*flySpeed);
            sp.setSy((float)(Math.cos(direction))*flySpeed);
            manager.addFancyStuff(sp);
        }
    }

    public static void createParticles(float x, float y, float tileSize, EntityManager man) {
        for (int i = 0; i < 3; i++) {
            int frame = (int)(Math.random()*3);
            Fanta f = new Fanta("stuff");
            Animation anim = new Animation(0, loadAnimationFromSpriteSheet("pebbles",frame,frame,3,1));
            f.putAnimation("stuff",anim);
            AnimatedParticle sp = new AnimatedParticle(f);
            sp.setSpeedZ(1+(float)(Math.random()*2));
            sp.setDuration(100+(float)(Math.random()*30));
            float wtf = tileSize;
            sp.setX(x+(float)(Math.random()*wtf)-wtf/2-4);
            sp.setY(y + (float) (Math.random() * wtf) - wtf / 2 - 4);
            sp.setFx(0.98f);
            sp.setFy(0.98f);
            float flySpeed = 1+(float)(Math.random()*3);
            float direction = (float)(Math.random()*Math.PI*2);
            sp.setSx((float)(Math.sin(direction))*flySpeed);
            sp.setSy((float)(Math.cos(direction))*flySpeed);
            man.addFancyStuff(sp);
        }
    }
}

package com.aa.ld29.logic;

/**
 * Created by Toni on 16.04.2014.
 */
import com.aa.ld29.gui.GameplayScreen;
import com.aa.ld29.logic.objects.enemies.Enemy;
import com.aa.ld29.logic.objects.Jumpy;
import com.aa.ld29.logic.objects.enemies.Puncher;
import com.aa.ld29.logic.objects.Spelunker;
import com.aa.ld29.logic.particle.AnimatedParticle;
import com.aa.ld29.logic.particle.ParticleList;
import com.aa.ld29.logic.particle.ParticleManager;
import com.aa.ld29.logic.tile.Tile;
import com.aa.ld29.logic.tile.TileManager;
import com.aa.ld29.maths.Collision;
import com.aa.ld29.maths.CollisionUtil;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.*;

/**
 * Manages all entities that are in the game at any given moment.
 *
 * @author Toni
 */
public class EntityManager {
    //Attribute
    private List<Entity> entities;
    private List<Entity> drawEntities;
    private List<Entity> inbox;
    private List<Entity> trash;
    private ParticleManager particles;
    private List<AnimatedParticle> fancyStuff;
    private List<Entity> bosses;
    private List<Entity> rewards;

    private List<Entity> players;

    private TileManager tileManager;

    private static final int tileSize = 32;
    private int screenWidth;
    private int screenHeight;

    private boolean victory = false;
    private int score = 0;
    private boolean multiplayer = false;

    private float screenShake;
    private float shakeReduction = 1f;
    private float shakeDampening = 0.99f;
    private float screenDisplacementX = 0;
    private float screenDisplacementY = 0;

    private Vector2 oldCenter = Vector2.Zero;
    
    private String helpText;

    private static long colorRandSeed;
    private static Comparator<Entity> depthComparator;

    static{
        Random random = new Random();
        colorRandSeed = random.nextLong();
        depthComparator = new Comparator<Entity>() {
            @Override
            public int compare(Entity o1, Entity o2) {
                if (o1.getLowerEdgeOfBoundingBox() == o2.getLowerEdgeOfBoundingBox()) return 0;
                return o1.getLowerEdgeOfBoundingBox() > o2.getLowerEdgeOfBoundingBox() ? -1 : 1;
            }
        };
    }

    //Konstruktor
    public EntityManager(int screenWidth, int screenHeight, boolean multiplayer) {
        entities = new ArrayList<>();
        drawEntities = new ArrayList<>();
        inbox = new ArrayList<>();
        trash = new ArrayList<>();
        particles = new ParticleList();
        fancyStuff = new ArrayList<>();
        bosses = new ArrayList<>();
        rewards = new ArrayList<>();

        players = new ArrayList<>();
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        this.multiplayer = multiplayer;
    }

    //Methoden
    public void createRandomMap(int stage){
        int xpos,ypos;
        int playerx = 2,playery;
        tileManager = TileManager.generateRandomMap(3, 16 + stage, 16 + stage, stage, tileSize);
        //place player
        playery = (int)(Math.random()*tileManager.getMapHeight());
        tileManager.setTile(playerx, playery, Tile.FLOOR);
        Vector2 pos = tileManager.getPositionAtIndex(playerx,playery);
        /*Entity player = EntityFactory.createPlayer(pos.x,pos.y);
        addEntity(player);
        players.add(player);*/
        createPlayer(playerx,playery,true);
        //place exit
        ypos = (int)(Math.random()*tileManager.getMapHeight());
        tileManager.setTile(tileManager.getMapWidth() - 2, ypos, Tile.FLOOR);
        pos = tileManager.getPositionAtIndex(tileManager.getMapWidth() - 2, ypos);
        Entity door = EntityFactory.createDoor(pos.x, pos.y);
        addEntity(door);
        //spawn bonus
        if(Math.random()<0.2) {
            xpos = (int) (Math.random() * (tileManager.getMapWidth() - 4)) + 2;
            ypos = (int) (Math.random() * (tileManager.getMapHeight() - 4)) + 2;
            tileManager.setTile(xpos, ypos, Tile.FLOOR);
            pos = tileManager.getPositionAtIndex(xpos, ypos);
            Entity healthpack = EntityFactory.createGoodie(pos.x,pos.y,2);
            addEntity(healthpack);
        }
        if(Math.random()<0.5) {
            xpos = (int) (Math.random() * (tileManager.getMapWidth() - 4)) + 2;
            ypos = (int) (Math.random() * (tileManager.getMapHeight() - 4)) + 2;
            tileManager.setTile(xpos, ypos, Tile.FLOOR);
            pos = tileManager.getPositionAtIndex(xpos, ypos);
            Entity healthpack = EntityFactory.createGoodie(pos.x,pos.y,3);
            addEntity(healthpack);
        }
        //spawn enemies
        for (int i = 0; i < stage; i++) {
            xpos = (int) (Math.random() * (tileManager.getMapWidth()));
            ypos = (int) (Math.random() * (tileManager.getMapHeight()));
            if(xpos == playerx && ypos == playery){
                xpos = tileManager.getMapWidth()-xpos;
            }
            tileManager.setTile(xpos, ypos, Tile.FLOOR);
            pos = tileManager.getPositionAtIndex(xpos, ypos);
            Entity e = EntityFactory.createEnemy(pos.x,pos.y,(int)(Math.random()*4)+1,false);
            //Entity e = EntityFactory.createEnemy(pos.x,pos.y,1);
            addEntity(e);
        }
    }

    public void createGoodie(int bonusTileX, int bonusTileY, int type, boolean reward) {
        Vector2 pos = tileManager.getPositionAtIndex(bonusTileX, bonusTileY);
        Entity goodie = EntityFactory.createGoodie(pos.x, pos.y,type);
        //placeEntityAt(door, exitTileX, exitTileY);
        tileManager.setTile(bonusTileX, bonusTileY, Tile.FLOOR);
        if(reward) {
            rewards.add(goodie);
        }else {
            addEntity(goodie);
        }
    }

    public void createEnemy(int enemyTileX, int enemyTileY, int type) {
        createEnemy(enemyTileX, enemyTileY, type, false);
    }
    public void createEnemy(int enemyTileX, int enemyTileY, int type, boolean boss) {
        Vector2 pos = tileManager.getPositionAtIndex(enemyTileX, enemyTileY);
        Entity e = EntityFactory.createEnemy(pos.x, pos.y, type, boss);
        if(boss){
            bosses.add(e);
        }
        placeEntityAt(e, enemyTileX, enemyTileY);
    }
    public void createExit(int exitTileX, int exitTileY){
        Vector2 pos = tileManager.getPositionAtIndex(exitTileX, exitTileY);
        Entity door = EntityFactory.createDoor(pos.x, pos.y);
        //placeEntityAt(door, exitTileX, exitTileY);
        tileManager.setTile(exitTileX, exitTileY, Tile.FLOOR);
        //entities.add(0,door);
        rewards.add(door);
    }
    public void createPlayer(int playerTileX, int playerTileY, boolean createSpace){
        Vector2 pos = tileManager.getPositionAtIndex(playerTileX,playerTileY);
        Spelunker player = (Spelunker)EntityFactory.createPlayer(pos.x,pos.y);
        placeEntityAt(player, playerTileX, playerTileY, createSpace);
        players.add(player);

        if(multiplayer) {
            Random colorRand = new Random(colorRandSeed);
            player.setDisplayHud(true);
            for (int i = 0; i < GameplayScreen.getGamepadCount(); i++) {
                Spelunker nextPlayer = (Spelunker) EntityFactory.createPlayer(pos.x, pos.y);
                nextPlayer.setKeyboardEnabled(false);
                nextPlayer.setGamepadIndex(i);
                nextPlayer.setColor(new Color(colorRand.nextFloat(), colorRand.nextFloat(), colorRand.nextFloat(), 1));
                nextPlayer.setDisplayHud(true);
                placeEntityAt(nextPlayer, playerTileX + i, playerTileY, false);
                players.add(nextPlayer);
            }
        }else{
            player.setGamepadIndex(0);
            player.setDisplayHud(false);
        }
    }
    public void placeEntityAt(Entity ent, int tilex, int tiley, boolean createSpace){
        placeEntityAt(ent, tilex, tiley);
        if(createSpace) {
            tileManager.setTile(tilex + 1, tiley, Tile.FLOOR);
            tileManager.setTile(tilex - 1, tiley, Tile.FLOOR);
            tileManager.setTile(tilex, tiley + 1, Tile.FLOOR);
            tileManager.setTile(tilex, tiley - 1, Tile.FLOOR);
        }
    }

    public void placeEntityAt(Entity ent, int tilex, int tiley){
        tileManager.setTile(tilex, tiley, Tile.FLOOR);
        addEntity(ent);
    }

    public void triggerLevelComplete() {
        //Show victory screen
        //load new level
        victory = true;
    }
    public boolean allEnemiesDead(){
        for (Entity entity : entities) {
            if(entity.isHostile()){
                return false;
            }
        }
        return true;
    }
    public void act(){
        checkForBossDefeat();

        addInbox();
        actEntities();
        actParticles();
        removeTrash();

        depthSortEntities();
        handleScreenshake();
    }

    private void depthSortEntities() {
        Collections.sort(drawEntities, depthComparator);
    }

    private void actParticles() {
        for (Iterator<AnimatedParticle> iterator = fancyStuff.iterator(); iterator.hasNext(); ) {
            AnimatedParticle ap = iterator.next();
            ap.act(this);
            tileManager.checkCollision(ap, this);
            if (!ap.isActive()) {
                iterator.remove();
            }
        }
        particles.act();
    }

    private void actEntities() {
        for (int i = 0; i < entities.size(); i++) {
            Entity ent1 = entities.get(i);
            for (int j = i+1; j < entities.size(); j++) {
                Entity ent2 = entities.get(j);
                Collision col = CollisionUtil.calcCollisionHitBoxes(ent1, ent2, true, false);
                if(ent1.collidesWith(ent2) && ent2.collidesWith(ent1) && col != null){
                    Entity.resolveCollision(col, !(ent1.isSpawnProtection() || ent2.isSpawnProtection()));
                }
                ent1.interactWith(ent2,col,this);
                ent2.interactWith(ent1,col,this);
            }
            ent1.act(this);
            tileManager.checkCollision(ent1,this);
            if(!ent1.isAlive()){
                killEntity(ent1);
            }
        }
        for (Entity ent1 : entities) {
            ent1.afterAct(this);
        }
    }

    private void addInbox() {
        if(!inbox.isEmpty()){
            addEntities(inbox);
            inbox.clear();
        }
    }

    private void removeTrash() {
        if(!trash.isEmpty()){
            removeEntities(trash);
            trash.clear();
        }
    }

    private void checkForBossDefeat() {
        boolean bossDefeated = true;
        for (int i = 0; i < bosses.size(); i++) {
            if(bosses.get(i).isAlive()){
                bossDefeated = false;
            }
        }
        if(bossDefeated && !rewards.isEmpty()){
            addEntities(rewards);
            rewards.clear();
        }
    }

    private void handleScreenshake(){
        screenShake -= shakeReduction;
        screenShake *= shakeDampening;
        if(screenShake < 0){
            screenShake = 0;
        }else{
            screenDisplacementX = (float)(Math.random()*screenShake)-screenShake/2;
            screenDisplacementY = (float)(Math.random()*screenShake)-screenShake/2;
        }
    }

    public void render(SpriteBatch batch){
        render(batch, oldCenter);
    }

    public void render(SpriteBatch batch, Vector2 cameraCenter){
        tileManager.render(batch, cameraCenter, screenWidth, screenHeight);
        for(Entity ent : drawEntities){
            if(!ent.isForeground()){
                ent.render(batch);
            }
        }
        for(AnimatedParticle animatedParticle : fancyStuff){
            animatedParticle.render(batch);
        }
        particles.render(batch);
        for(Entity ent : drawEntities){
            if(ent.isForeground()){
                ent.render(batch);
            }
        }
    }

    private void addEntity(Entity ent){
        entities.add(ent);
        drawEntities.add(ent);
    }

    private void addEntities(List<Entity> ents){
        entities.addAll(ents);
        drawEntities.addAll(ents);
    }
    private void removeEntities(List<Entity> ents){
        entities.removeAll(ents);
        drawEntities.removeAll(ents);
    }

    public void spawnEntity(Entity ent){
        inbox.add(ent);
    }
    public void killEntity(Entity ent){
        ent.die(this);
        trash.add(ent);
    }
    //Stuff
    public void setImportantInfo(List<PlayerInfo> infos){
        for (int i = 0; i < infos.size(); i++) {
            PlayerInfo info = infos.get(i);
            Spelunker player = ((Spelunker) this.getPlayers().get(i));
            this.score = info.score;
            player.setHealth(info.hp);
            player.setSpecialAmmo(info.specialAmmo);
            player.setCurrentSpecial(info.currentSpecial);
        }
    }

    public void addScore(int score){
        this.score += score;
    }

    public void createExplosion(float x, float y, float power, float radius){
        tileManager.destroyTilesAt(x,y,radius,this);
        for (Entity ent1 : entities) {
            if (ent1.getCenter().dst(x, y) < radius && ent1.isSolid()) {
                if (!(ent1 instanceof Enemy) || !((Enemy) ent1).isUnderground()) {
                    ent1.damage(power);
                    Vector2 delta = ent1.getCenter().sub(x, y);
                    float direction = (float) Math.atan2(delta.x, delta.y);
                    ent1.addSpeed((float) (Math.sin(direction) * power), (float) (Math.cos(direction) * power));
                    if (ent1 instanceof Jumpy) {
                        ((Jumpy) ent1).setSpeedZ(1 + (float) (Math.random()));
                    }
                }
            }
        }
        //visual effect
        EntityFactory.createExplosionParticles(x, y, radius, this);
        //screen shake
        screenShake += power*5;
    }

    public void punchPeopleInTheFace(float x, float y, float hitRange, float damage, float pushBack, Puncher source) {
        for (Entity ent1 : entities) {
            if (ent1 != source && source.canPunchInTheFace(ent1) && ent1.getCenter().dst(x, y) < hitRange && ent1.isSolid()) {
                ent1.damage(damage);
                Vector2 delta = ent1.getCenter().sub(x, y);
                float direction = (float) Math.atan2(delta.x, delta.y);
                ent1.addSpeed((float) (Math.sin(direction) * pushBack), (float) (Math.cos(direction) * pushBack));
                if (ent1 instanceof Jumpy) {
                    ((Jumpy) ent1).setSpeedZ(1 + (float) (Math.random()));
                }
            }
        }
    }

    //Getter & Setter
    public void addFancyStuff(AnimatedParticle p){
        fancyStuff.add(p);
    }

    public List<Entity> getPlayers() {
        return players;
    }

    public List<Entity> getAliveTargets(){
        List<Entity> alivePlayers = new ArrayList<Entity>();
        for (Entity player : players) {
            if(player.isAlive()){
                alivePlayers.add(player);
            }
        }
        return alivePlayers;
    }

    public ParticleManager getParticles() {
        return particles;
    }

    public List<Entity> getEntities() {
        return entities;
    }

    public int getLevelWidth(){
        return tileManager.getTotalWidth();
    }
    public int getLevelHeight(){
        return tileManager.getTotalHeight();
    }

    public boolean isVictory() {
        return victory;
    }

    public int getScore() {
        return score;
    }

    private OrthographicCamera getCameraCenteredOn(Vector2 center){
        OrthographicCamera camera = new OrthographicCamera(screenWidth, screenHeight);
        camera.translate(center.x, center.y);
        camera.update();
        return camera;
    }

    public OrthographicCamera getMedianCamera() {
        return getCameraCenteredOn(calculateMedianCameraCenter(players));
    }

    public OrthographicCamera getPessimisticCamera(){
        Vector2 center = Vector2.Zero;

        List<Entity> currentList = new ArrayList<>();

        for(Entity ent : players){
            if(ent.isAlive()){
                currentList.add(ent);
                Rectangle rect = calculateRect(currentList);
                if(rect.width > screenWidth*0.6 || rect.height > screenHeight*0.6){
                    currentList.remove(ent);
                }
            }
        }

        return getCameraCenteredOn(calculateMedianCameraCenter(currentList));
    }

    private Rectangle calculateRect(List<Entity> entities){
        float minx = Float.MAX_VALUE,miny = Float.MAX_VALUE,maxx = Float.MIN_VALUE,maxy = Float.MIN_VALUE;
        for(Entity ent : entities){
            Vector2 center = ent.getCenter();
            minx = Math.min(minx,center.x);
            maxx = Math.max(maxx,center.x);
            miny = Math.min(miny,center.y);
            maxy = Math.max(maxy,center.y);
        }

        if(entities.isEmpty()){
            return new Rectangle(0,0,0,0);
        }else{
            return new Rectangle(minx,miny,maxx-minx,maxy-miny);
        }
    }

    public OrthographicCamera getPlayerCamera(int playerIndex){
        return getCameraCenteredOn(players.get(playerIndex).getCenter());
    }

    private Vector2 calculateMedianCameraCenter(List<Entity> entities){
        Vector2 screenCenter = oldCenter;

        findMedianCenter(entities,screenCenter);

        if (screenCenter.x < screenWidth / 2 - tileSize) {
            screenCenter.x = screenWidth / 2 - tileSize;
        } else if (screenCenter.x > getLevelWidth() - screenWidth / 2 + tileSize) {
            screenCenter.x = getLevelWidth() - screenWidth / 2 + tileSize;
        }
        if (screenCenter.y < screenHeight / 2 - tileSize) {
            screenCenter.y = screenHeight / 2 - tileSize;
        } else if (screenCenter.y > getLevelHeight() - screenHeight / 2 + tileSize) {
            screenCenter.y = getLevelHeight() - screenHeight / 2 + tileSize;
        }
        if (getLevelWidth() < screenWidth) {
            screenCenter.x = getLevelWidth() / 2;
        }
        if (getLevelHeight() < screenHeight) {
            screenCenter.y = getLevelHeight() / 2;
        }
        screenCenter.add(screenDisplacementX, screenDisplacementY);
        oldCenter = screenCenter;
        return screenCenter;
    }

    private void findMedianCenter(List<Entity> entities, Vector2 currentCenter){
        int cameraFocusCount = 2;
        for(Entity ent : entities){
            if(ent.isAlive()){
                cameraFocusCount++;
                Vector2 diff = ent.getCenter().sub(currentCenter);
                currentCenter.add(diff.scl(1f / (float) cameraFocusCount));
            }
        }
    }

    public void setTileManager(TileManager tileManager) {
        this.tileManager = tileManager;
    }

    public TileManager getTileManager() {
        return tileManager;
    }

    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    public String getHelpText() {
        return helpText;
    }

    public void setScreenSize(int screenWidth, int screenHeight){
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }
}

package com.aa.ld29.logic.objects;

import com.aa.ld29.dal.audio.SoundManager;
import com.aa.ld29.dal.input.ControlSettings;
import com.aa.ld29.dal.audio.SoundFX;
import com.aa.ld29.dal.TextureManager;
import com.aa.ld29.dal.input.InputAction;
import com.aa.ld29.gui.GameplayScreen;
import com.aa.ld29.logic.*;
import com.aa.ld29.logic.objects.bombs.Bomb;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import static com.aa.ld29.dal.input.InputAction.*;

/**
 * Created by Toni on 14.04.2014.
 */
public class Spelunker extends BasicObject {
    private float runAccel = 0.18f;

    private float throwCooldown = 30;
    private float throwTimer = throwCooldown;

    private float bombThrowSpeed = 2.8f;
    private float specialThrowSpeed = 2.3f;

    private Bomb holding;
    private int throwType;

    private int currentSpecial = 0;
    private int[] specialAmmo = new int[SPECIAL_TYPES.length];

    private boolean fixedInScene;
    private Vector2 targetPosition;
    private float sceneMoveSpeed = 0.9f;

    public static final String[] SPECIAL_ICONS = new String[] {"dynamite_icon","cluster_icon","schnirdlkrocha_icon","mine_icon","sticky_icon"};
    public static final int[] SPECIAL_TYPES = new int[] {2,3,5,6,7};

    private static BitmapFont hudFont;
    private boolean displayHud;

    private int gamepadIndex = -1;
    private boolean keyboardEnabled = true;

    public Spelunker(Fanta fanta) {
        super(fanta);
        setHurtSound(SoundFX.HURT_2);
    }

    static{
        initFont();
    }

    private static void initFont(){
        hudFont = new BitmapFont(Gdx.files.internal("fonts/press_start_2p.fnt"),Gdx.files.internal("fonts/press_start_2p_0.png"),false);
        hudFont.setColor(Color.WHITE);
        hudFont.setFixedWidthGlyphs("0123456789");
        hudFont.setScale(0.25f, 0.5f);
    }

    public static String getSpecialIcon(int index) {
        return SPECIAL_ICONS[index];
    }


    @Override
    public void act(EntityManager man) {
        hudFadeDelay--;
        if(fixedInScene){
            getFanta().act(1);
            Vector2 delta = targetPosition.sub(getCenter());
            float direction = (float)(Math.atan2(delta.x,delta.y));
            float movex = (float)(Math.sin(direction)* sceneMoveSpeed);
            addPosition(movex,(float)(Math.cos(direction)* sceneMoveSpeed));
            if(movex > 0){
                setFlipX(true);
            }else{
                setFlipX(false);
            }
            this.rescale(0.96f,0.96f);
        }else {
            super.act(man);

            Vector2 movement = getMoveVector();

            String animation = "standing";
            if (movement.x != 0 || movement.y != 0) {
                addSpeed(movement);
                if (movement.x < 0) {
                    setFlipX(true);
                } else if (movement.x > 0) {
                    setFlipX(false);
                }
                animation = "running";
            }

            if (isSwitchSpecialPressed()) {
                switchSpecial();
            }

            if (holding == null) {
                if (throwType == -1) {
                    if (!isBombButtonDown() && !isSpecialButtonDown()) {
                        throwType = 0;
                    }
                } else {
                    Bomb bomb;
                    if (isBombButtonDown() && canThrowBomb()) {
                        throwTimer = 0;

                        bomb = EntityFactory.createBomb(getCenter().x, getCenter().y, this, 1);
                        holding = bomb;
                        holding.pickUp(this);
                        throwType = 1;
                    } else if (isSpecialButtonDown() && canThrowSpecial()) {
                        throwTimer = 0;
                        specialAmmo[currentSpecial]--;

                        bomb = EntityFactory.createBomb(getCenter().x, getCenter().y, this, SPECIAL_TYPES[currentSpecial]);
                        holding = bomb;
                        holding.pickUp(this);
                        throwType = 2;
                    }
                }
            } else {
                if (!holding.isAlive()) {
                    holding = null;
                    throwTimer = 0;
                    throwType = -1;
                } else {
                    holding.setCenter(this.getCenter());
                    holding.act(man);
                    if (throwType == 1 && !isBombButtonDown()) {
                        throwTimer = 0;
                        throwType = 0;
                        SoundManager.getInstance().play(SoundFX.THROW);

                        holding.setSx(getSx() * bombThrowSpeed);
                        holding.setSy(getSy() * bombThrowSpeed);
                        man.spawnEntity(holding);
                        holding.throwAway();
                        holding = null;
                    } else if (throwType == 2 && !isSpecialButtonDown()) {
                        throwTimer = 0;
                        throwType = 0;
                        SoundManager.getInstance().play(SoundFX.THROW);

                        holding.setSx(getSx() * specialThrowSpeed);
                        holding.setSy(getSy() * specialThrowSpeed);
                        man.spawnEntity(holding);
                        holding.throwAway();
                        holding = null;
                    }
                }
            }
            throwTimer++;

            getFanta().setCurrentAnimation(animation);
        }
    }

    private Vector2 getMoveVector(){
        ControlSettings controls = ControlSettings.getInstance();
        Vector2 movement = new Vector2(0,0);
        if(keyboardEnabled) {
            if (controls.isDown(ACTION_MOVE_LEFT)) {
                movement.add(-runAccel, 0);
            }
            if (controls.isDown(ACTION_MOVE_RIGHT)) {
                movement.add(runAccel, 0);
            }
            if (controls.isDown(ACTION_MOVE_UP)) {
                movement.add(0, runAccel);
            }
            if (controls.isDown(ACTION_MOVE_DOWN)) {
                movement.add(0, -runAccel);
            }
        }
        //insert gamepad here
        if(GameplayScreen.isGamepadAvailable(gamepadIndex)) {
            Controller gamepad = GameplayScreen.getGamepad(gamepadIndex);
            if (movement.x == 0 && movement.y == 0) {
                movement.x = gamepad.getAxis(1);
                movement.y = -gamepad.getAxis(0);
                float deadzone = 0.15f;
                if (movement.len() < deadzone) {
                    movement.x = gamepad.getAxis(3);
                    movement.y = -gamepad.getAxis(2);
                    if (movement.len() < deadzone) {
                        movement.x = 0;
                        movement.y = 0;
                    }
                }
            }
        }
        movement.limit(runAccel);
        return movement;
    }

    private boolean isBombButtonDown(){
        boolean button = false;
        if(keyboardEnabled) {
            button = ControlSettings.getInstance().isDown(ACTION_THROW_BOMB);
        }
        if(GameplayScreen.isGamepadAvailable(gamepadIndex)) {
            if (!button) {
                button = GameplayScreen.getGamepad(gamepadIndex).getButton(0) || GameplayScreen.getGamepad(gamepadIndex).getButton(2);
            }
        }
        return button;
    }
    private boolean isSpecialButtonDown(){
        boolean button = false;
        if(keyboardEnabled) {
            button = ControlSettings.getInstance().isDown(ACTION_THROW_SPECIAL);
        }
        if(GameplayScreen.isGamepadAvailable(gamepadIndex)) {
            if (!button) {
                button = GameplayScreen.getGamepad(gamepadIndex).getButton(1) || GameplayScreen.getGamepad(gamepadIndex).getButton(3);
            }
        }
        return button;
    }
    private boolean isSwitchSpecialPressed(){
        boolean button = false;
        if(keyboardEnabled){
            button = ControlSettings.getInstance().isPressed(InputAction.ACTION_SWITCH_SPECIAL);
        }
        if(GameplayScreen.isGamepadAvailable(gamepadIndex)){
            if(!button){
                button = GameplayScreen.getGamepad(gamepadIndex).getPov(0) == PovDirection.east;
            }
        }
        return button;
    }

    @Override
    public void die(EntityManager man) {
        super.die(man);
        if(holding != null){
            man.spawnEntity(holding);
            holding.throwAway();
            holding = null;
        }
    }

    @Override
    public boolean isForeground() {
        return false;
    }

    @Override
    public void render(SpriteBatch batch) {
        float shadowSize = (16*(getImgW()/32));
        batch.draw(TextureManager.getInstance().getTexture("shadow"), getCenter().x-(shadowSize)/2, getY() - 3,shadowSize,shadowSize);
        super.render(batch);
        if(holding != null){
            holding.render(batch);
        }
        if(shouldRenderInformation()){
            for (int i = 0; i < getHealth(); i++) {
                batch.draw(TextureManager.getInstance().getTexture("heart"),getX()+i*4,getY()-12,8,8);
            }
            batch.draw(TextureManager.getInstance().getTexture(Spelunker.getSpecialIcon(getCurrentSpecial())), getX(), getY()-24, 16, 16);
            hudFont.draw(batch, "x" + getCurrentSpecialAmmo(), getX() + 18, getY() - 14);
        }
    }

    @Override
    public void damage(float damage) {
        hudFadeDelay = 30;
        super.damage(damage);
    }

    private int hudFadeDelay = 60;

    private boolean shouldRenderInformation(){
        return displayHud && hudFadeDelay > 0;
    }

    private boolean canThrowSpecial() { return canThrowBomb() && specialAmmo[currentSpecial] > 0; }

    private boolean canThrowBomb(){
        return throwTimer >= throwCooldown;
    }

    private void switchSpecial(){
        currentSpecial++;
        if(currentSpecial>= SPECIAL_TYPES.length){
            currentSpecial = 0;
        }
        hudFadeDelay = 30;
    }

    public int[] getSpecialAmmo() {
        return specialAmmo;
    }

    public int getCurrentSpecial(){
        return this.currentSpecial;
    }

    public int getCurrentSpecialAmmo() {
        return this.specialAmmo[currentSpecial];
    }

    public void addAmmo(int ammo, int ammoType) {
        this.specialAmmo[ammoType] += ammo;
    }

    public void setSpecialAmmo(int[] specialAmmo) {
        this.specialAmmo = specialAmmo;
    }

    public void setCurrentSpecial(int currentSpecial) {
        this.currentSpecial = currentSpecial;
    }

    public float getRunAccel() {
        return runAccel;
    }

    public void setRunAccel(float runAccel) {
        this.runAccel = runAccel;
    }

    public void setFixedInScene(boolean fixedInScene) {
        this.fixedInScene = fixedInScene;
    }

    public boolean isFixedInScene() {
        return fixedInScene;
    }

    public void setTargetPosition(Vector2 targetPosition) {
        this.targetPosition = targetPosition;
    }

    public void setGamepadIndex(int gamepadIndex) {
        this.gamepadIndex = gamepadIndex;
    }

    public void setKeyboardEnabled(boolean keyboardEnabled) {
        this.keyboardEnabled = keyboardEnabled;
    }

    public void setDisplayHud(boolean displayHud) {
        this.displayHud = displayHud;
    }
}

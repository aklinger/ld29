package com.aa.ld29.logic.objects;

import com.aa.ld29.logic.BasicObject;
import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.Fanta;
import com.aa.ld29.logic.objects.bombs.Bomb;
import com.aa.ld29.logic.objects.enemies.Enemy;
import com.aa.ld29.maths.Collision;
import com.aa.ld29.maths.HitBox;

/**
 * Created by Toni on 26.04.2014.
 */
public class Bullet extends BasicObject{

    private float direction;
    private float speed;
    private float timer = 0;
    private float duration;
    private float damage = 1;

    private Entity father;

    private boolean alive = true;

    public Bullet (Fanta f, Entity father){
        super(f);
        this.father = father;
        setSolid(false);
    }

    @Override
    public void act(EntityManager man) {
        super.act(man);
        addPosition((float)(Math.sin(direction)*speed),(float)(Math.cos(direction)*speed));
        timer++;
        setRotation((float)(-direction/Math.PI*180)-90);
        if(timer > duration){
            alive = false;
        }
    }

    @Override
    public void interactWith(Entity ent2, Collision col, EntityManager man) {
        if(col != null && ent2.isSolid() && ent2 != father && !(ent2 instanceof Bomb) && (!(ent2 instanceof Enemy) || !((Enemy)ent2).isUnderground())){
            ent2.damage(damage);
            alive = false;
        }
    }

    @Override
    public void damage(float damage) {
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    @Override
    public int getType() {
        return HitBox.CIRCLE;
    }

    //Getter & Setter
    public void setDirection(float direction) {
        this.direction = direction;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }
}

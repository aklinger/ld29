package com.aa.ld29.logic.objects;

import com.aa.ld29.dal.TextureManager;
import com.aa.ld29.logic.BasicObject;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.Fanta;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Toni on 27.04.2014.
 */
public class Jumpy extends BasicObject{

    private float elevation;
    private float speedZ;
    private float gravZ = 0.1f;
    private float bounZ = 0.5f;
    private float stiction = 0.8f;

    private boolean awesomeEffect = true;

    public Jumpy(Fanta fanta) {
        super(fanta);
    }

    @Override
    public void act(EntityManager man) {
        super.act(man);
        if(awesomeEffect) {
            speedZ -= gravZ;
            elevation += speedZ;
            if (elevation < 0) {
                touchGround();
            }
        }
    }

    protected void touchGround(){
        setSx(getSx() * stiction);
        setSy(getSy() * stiction);
        speedZ *= -bounZ;
        if (speedZ < 0) {
            speedZ = 0;
        }
        elevation = 0;
    }

    @Override
    public void render(SpriteBatch batch) {
        if(awesomeEffect) {
            batch.draw(TextureManager.getInstance().getTexture("shadow2"), getX() + getImgW() / 2 - 4, getLowerEdgeOfBoundingBox() - 1);
            setY(getY() + elevation);
            super.render(batch);
            setY(getY() - elevation);
        }else{
            super.render(batch);
        }
    }

    public float getElevation() {
        return elevation;
    }

    public void setElevation(float elevation) {
        this.elevation = elevation;
    }

    public void setSpeedZ(float speedZ) {
        this.speedZ = speedZ;
    }

    public void setBounZ(float bounZ) {
        this.bounZ = bounZ;
    }

    public void setGravZ(float gravZ) {
        this.gravZ = gravZ;
    }

    public void setStiction(float stiction) {
        this.stiction = stiction;
    }

    public void setAwesomeEffect(boolean awesomeEffect) {
        this.awesomeEffect = awesomeEffect;
    }
}

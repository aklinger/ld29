package com.aa.ld29.logic.objects.bombs;

import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.Fanta;
import com.aa.ld29.logic.objects.enemies.Enemy;
import com.aa.ld29.maths.Collision;

/**
 * Created by Toni on 02.08.2014.
 */
public class Sticky extends Bomb{

    private boolean sticky = true;
    private Entity target;

    private float offsetx;
    private float offsety;

    public Sticky(Fanta fanta, Entity cause, float fuseDuration, float radius, float damage) {
        super(fanta, cause, fuseDuration, radius, damage);
    }

    @Override
    public void act(EntityManager man) {
        super.act(man);
        if(target != null){
            this.setX(target.getX()+offsetx);
            this.setY(target.getY()+offsety);
            if(target instanceof Enemy){
                if(((Enemy) target).isUnderground()){
                    target = null;
                }
            }
        }
    }

    @Override
    public void interactWith(Entity ent2, Collision col, EntityManager man) {
        super.interactWith(ent2, col, man);
        if(sticky && col != null && ent2 != getCause()){
            target = ent2;
            offsetx = getX()-ent2.getX();
            offsety = getY()-ent2.getY();
        }
    }

    @Override
    protected void touchGround() {
        super.touchGround();
        sticky = false;
    }

    @Override
    public void throwAway() {
        super.throwAway();
        sticky = true;
    }
}

package com.aa.ld29.logic.objects.bombs;

import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityFactory;
import com.aa.ld29.logic.Fanta;

/**
 * Created by Toni on 01.08.2014.
 */
public class SchnirdlKrocha extends ClusterBomb {

    private int chainCount = 5;
    private float chainFuseDuration;

    public SchnirdlKrocha(Fanta fanta, Entity cause, float fuseDuration, float radius, float damage, float explodeSpeedMin, float explodeSpeedMax, int chainCount, float chainFuseDuration) {
        super(fanta, cause, fuseDuration, radius, damage, 1, explodeSpeedMin, explodeSpeedMax);
        this.chainCount = chainCount;
        this.chainFuseDuration = chainFuseDuration;
    }

    @Override
    protected Entity getShrapnel() {
        if(chainCount > 0) {
            Entity ent = EntityFactory.createBomb(getCenter().x, getCenter().y,getCause(), 5);
            ((SchnirdlKrocha)ent).chainCount = chainCount -1;
            ((SchnirdlKrocha)ent).setFuseDuration(chainFuseDuration);
            return ent;
        }else{
            return null;
        }
    }
}

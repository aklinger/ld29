package com.aa.ld29.logic.objects.bombs;

import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityFactory;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.Fanta;
import com.aa.ld29.logic.objects.bombs.Bomb;
import com.aa.ld29.maths.Calc;

/**
 * Created by Toni on 29.07.2014.
 */
public class ClusterBomb extends Bomb {

    private int shrapnel_count;
    private float explodeSpeedMin = 0.1f;
    private float explodeSpeedMax = 7.5f;

    public ClusterBomb(Fanta fanta, Entity cause,float fuseDuration, float radius, float damage, int shrapnel_count, float explodeSpeedMin, float explodeSpeedMax) {
        super(fanta, cause, fuseDuration, radius, damage);
        this.shrapnel_count = shrapnel_count;
        this.explodeSpeedMin = explodeSpeedMin;
        this.explodeSpeedMax = explodeSpeedMax;
    }

    @Override
    public void explode(EntityManager man) {
        super.explode(man);
        double rotation = Calc.TWO_PI*Math.random();
        double part = Calc.TWO_PI/shrapnel_count;
        for (int i = 0; i < shrapnel_count; i++) {
            Entity ent = getShrapnel();
            if(ent != null) {
                ent.setSx((float) (Math.sin(rotation) * (explodeSpeedMin + (explodeSpeedMax - explodeSpeedMin) * Math.random())));
                ent.setSy((float) (Math.cos(rotation) * (explodeSpeedMin + (explodeSpeedMax - explodeSpeedMin) * Math.random())));
                man.spawnEntity(ent);
                rotation += part;
            }else{
                break;
            }
        }
    }

    protected Entity getShrapnel(){
        return EntityFactory.createBomb(getCenter().x, getCenter().y,getCause(), 4);
    }
}

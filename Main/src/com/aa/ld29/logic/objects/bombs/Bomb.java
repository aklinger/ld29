package com.aa.ld29.logic.objects.bombs;

import com.aa.ld29.dal.audio.SoundFX;
import com.aa.ld29.dal.audio.SoundManager;
import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.Fanta;
import com.aa.ld29.logic.objects.Jumpy;

/**
 * Created by Toni on 26.04.2014.
 */
public class Bomb extends Jumpy {

    private float timer;
    private float fuseDuration;
    private float radius;
    private float damage;
    private float ITSGONNABLOW = 60;

    private String[] explosionSounds = SoundFX.LIGHT_EXPLOSIONS;

    private Entity pickedUpBy;
    private Entity cause;

    private boolean alive = true;

    public Bomb (Fanta fanta, Entity cause, float fuseDuration, float radius, float damage){
        super(fanta);
        this.fuseDuration = fuseDuration;
        this.timer = 0;
        this.radius = radius;
        this.damage = damage;
        this.cause = cause;
    }

    @Override
    public void act(EntityManager man) {
        super.act(man);
        burnFuse(man);
        if(pickedUpBy != null && !pickedUpBy.isAlive()){
            throwAway();
        }
    }

    protected void burnFuse(EntityManager man){
        timer++;
        if(timer >= fuseDuration){
            explode(man);
        }else if(timer >= fuseDuration-ITSGONNABLOW){
            getFanta().setCurrentAnimation("aboutToExplode");
        }
    }

    protected void resetTimer(){
        timer = 0;
    }

    public float timeUntilExplosion(){
        return fuseDuration - timer;
    }

    @Override
    public boolean collidesWith(Entity other) {
        /*if(super.collidesWith(other) && !(other instanceof Spelunker)){
            return true;
        }*/
        return false;
    }

    protected void explode(EntityManager man){
        //GO BOOM
        SoundManager.getInstance().playRandom(explosionSounds);
        man.createExplosion(getCenter().x,getCenter().y,damage,radius);

        alive = false;
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    public void pickUp(Entity entity){
        pickedUpBy = entity;
        setAwesomeEffect(false);
    }

    public void throwAway(){
        cause = pickedUpBy;
        pickedUpBy = null;
        setAwesomeEffect(true);
    }

    public Entity isPickedUpBy() {
        return pickedUpBy;
    }

    public void setExplosionSounds(String[] explosionSounds) {
        this.explosionSounds = explosionSounds;
    }

    protected void setFuseDuration(float fuseDuration){
        this.fuseDuration = fuseDuration;
    }

    public Entity getCause() {
        return cause;
    }
}

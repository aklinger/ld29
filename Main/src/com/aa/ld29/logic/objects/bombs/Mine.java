package com.aa.ld29.logic.objects.bombs;

import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.Fanta;
import com.aa.ld29.logic.objects.enemies.Enemy;

/**
 * Created by Toni on 02.08.2014.
 */
public class Mine extends Bomb{

    private float triggerRange;
    private boolean focused = false;
    private boolean triggered = false;

    public Mine(Fanta fanta, Entity cause, float fuseDuration, float radius, float damage, float triggerRange) {
        super(fanta, cause, fuseDuration, radius, damage);
        this.triggerRange = triggerRange;
        setBounZ(0);
    }

    @Override
    protected void burnFuse(EntityManager man) {
        if(focused){
            if(getElevation()>0){
                focused = false;
                getFanta().setCurrentAnimation("thrown");
                resetTimer();
            }else if(triggered){
                super.burnFuse(man);
            }else {
                for (int i = 0; i < man.getEntities().size(); i++) {
                    Entity entity = man.getEntities().get(i);
                    if (entity instanceof Enemy && !((Enemy) entity).isUnderground()) {
                        float dist = entity.getCenter().dst(getCenter());
                        if (dist < triggerRange) {
                            getFanta().setCurrentAnimation("aboutToExplode");
                            triggered = true;
                            break;
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void touchGround() {
        super.touchGround();
        if(!focused) {
            focused = true;
            getFanta().setCurrentAnimation("focus");
        }
    }

    @Override
    public void pickUp(Entity entity) {
        super.pickUp(entity);
        getFanta().setCurrentAnimation("held");
    }

    @Override
    public void throwAway() {
        super.throwAway();
        getFanta().setCurrentAnimation("thrown");
    }
}

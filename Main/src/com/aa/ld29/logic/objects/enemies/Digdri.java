package com.aa.ld29.logic.objects.enemies;

import com.aa.ld29.dal.audio.SoundFX;
import com.aa.ld29.dal.audio.SoundManager;
import com.aa.ld29.logic.*;
import com.aa.ld29.logic.objects.Bullet;
import com.aa.ld29.logic.objects.enemies.bossfights.DigdriSetting;
import com.aa.ld29.maths.CollisionUtil;

/**
 * Created by Toni on 26.04.2014.
 */
public class Digdri extends Enemy{

    private DigdriSetting setting;

    private float timer = 0;
    private boolean aggro = false;
    private int extraShots = 1;
    private int extraShotCounter = extraShots;

    public Digdri (Fanta f){
        super(f);
        setting = new DigdriSetting();
        setting.shotType = 1;
        setting.aggroRange = 105;
        setting.cooldown = 60*1.3f;
        setting.streuung = 0.2f;
        //
        timer = setting.cooldown-20;
        //
        setCollisionDamage(0);
        setFx(0.9f);
        setFy(0.9f);
    }

    private String nextAnimation;

    @Override
    public void act(EntityManager man) {
        nextAnimation = getFanta().getCurrentAnimation();
        super.act(man);
        aggro = false;
        boolean shotOnce = false;
        if(timer > setting.cooldown) {
            float minDist = Float.MAX_VALUE;
            Entity closestPlayer = null;

            for (int i = 0; i < man.getAliveTargets().size(); i++) {
                Entity player = man.getAliveTargets().get(i);
                if (player.getCenter().dst(getCenter()) < minDist) {
                    minDist = player.getCenter().dst(getCenter());
                    closestPlayer = player;
                }
            }
            if(closestPlayer != null){
                if (closestPlayer.getCenter().dst(getCenter()) < setting.aggroRange) {
                    aggro = true;
                    extraShotCounter = 0;
                }
                if(aggro || getFanta().getCurrentAnimation().equals("normal")){
                    if(getFanta().getCurrentAnimation().equals("standing")){
                        nextAnimation = "emerging";
                        getFanta().rewindCurrentAnimation();

                        setSx(getSx() * 0.01f);
                        setSy(getSy() * 0.01f);
                        setFixated(true);
                        setSpawnProtection(2);
                    }else{
                        if(getFanta().getCurrentAnimation().equals("emerging") && !(getFanta().isAnimationFinished())) {
                            nextAnimation = "emerging";
                        }else {
                            Bullet b = EntityFactory.createBullet(getCenter().x, getCenter().y, setting.shotType, this);
                            float diversification = (float) (Math.random() * setting.streuung) - setting.streuung / 2;
                            b.setDirection(CollisionUtil.directionTo(getCenter(), closestPlayer.getCenter()) + diversification);
                            man.spawnEntity(b);
                            if (closestPlayer.getX() - getX() > 0) {
                                setFlipX(true);
                            } else {
                                setFlipX(false);
                            }
                            timer = 0;
                            nextAnimation = "shooting";
                            getFanta().rewindCurrentAnimation();
                            SoundManager.getInstance().play(SoundFX.SHOOT);
                            if(!aggro) {
                                extraShotCounter++;
                            }
                        }
                    }
                }
            }
        }
        timer++;
        getFanta().setCurrentAnimation(nextAnimation);
    }

    @Override
    protected void randomMovementBehaviour() {
        if(!aggro && timer > setting.cooldown && extraShotCounter >= extraShots) {
            if(!getFanta().getCurrentAnimation().equals("standing") && !getFanta().getCurrentAnimation().equals("digging"))
            {
                nextAnimation = "digging";
                getFanta().setCurrentAnimation("digging");
                getFanta().rewindCurrentAnimation();
            }else if(getFanta().isAnimationFinished()){
                nextAnimation = "standing";
                setFixated(false);
            }
            if(!isFixated()) {
                super.randomMovementBehaviour();
            }
        }else{
            if(getFanta().isAnimationFinished()){
                nextAnimation = "normal";
            }
        }
    }

    protected void setExtraShots(int extraShots) {
        this.extraShots = extraShots;
    }

    public void setAggroRange(float aggroRange) {
        this.setting.aggroRange = aggroRange;
    }

    public void setCooldown(float cooldown) {
        this.setting.cooldown = cooldown;
    }

    public void setShotType(int shotType) {
        this.setting.shotType = shotType;
    }

    public void setSetting(DigdriSetting setting) {
        this.setting = setting;
    }
}

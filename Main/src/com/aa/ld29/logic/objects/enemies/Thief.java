package com.aa.ld29.logic.objects.enemies;

import com.aa.ld29.dal.audio.SoundFX;
import com.aa.ld29.dal.audio.SoundManager;
import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.Fanta;
import com.aa.ld29.logic.objects.bombs.Bomb;
import com.aa.ld29.maths.Collision;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 26.04.2014.
 */
public class Thief extends Enemy{

    private float aggroRange = 220;
    private float maxThrowRange = 100;
    private float minThrowRange = 80;
    private float throwSpeed = 6;
    private float throwRemainingTime = 40;
    private float throwPoint = 7f;

    private float chaseSpeed = 1.337f;
    private float collectSpeed = 1.9f;

    private float holdDuration = 60*3f;
    private float holdTimer;

    private float cooldown = 60*1f;
    private float timer = 0;
    private boolean throwing;
    private Entity target;

    private Bomb collected;

    public Thief (Fanta f){
        super(f);
        setFx(0.95f);
        setFy(0.95f);
        setHurtSound(SoundFX.HURT_3);
    }

    private String nextAnimation;

    @Override
    public void act(EntityManager man) {
        nextAnimation = getFanta().getCurrentAnimation();
        super.act(man);
        if(throwing) {
            if(timer >= throwPoint){
                Vector2 delta = target.getCenter().sub(getCenter());
                float speedX = (float) (Math.sin(Math.atan2(delta.x, delta.y)) * throwSpeed);
                collected.setSx(speedX);
                collected.setSy((float) (Math.cos(Math.atan2(delta.x, delta.y)) * throwSpeed));
                collected.setSpeedZ(1 + (float) (Math.random()));
                collected.setElevation(3);
                collected.throwAway();
                SoundManager.getInstance().play(SoundFX.THROW);
                collected = null;
                target = null;
                throwing = false;
            }
        }else if(collected != null) {
            holdTimer++;
            nextAnimation = "carrying";
            collected.setCenter(getCenter().x,getCenter().y+3);
            collected.setSx(0);
            collected.setSy(0);
            float minDist = Float.MAX_VALUE;
            Entity minPlayer = null;

            for (int i = 0; i < man.getAliveTargets().size(); i++) {
                Entity player = man.getAliveTargets().get(i);
                if (player.getCenter().dst(getCenter()) < minDist) {
                    minDist = player.getCenter().dst(getCenter());
                    minPlayer = player;
                }
            }
            if(minPlayer != null) {
                Vector2 delta = minPlayer.getCenter().sub(getCenter());
                if (minDist > maxThrowRange) {
                    move((float)(Math.atan2(delta.x,delta.y)),chaseSpeed);
                } else if(minDist < minThrowRange){
                    move((float)(Math.atan2(delta.x,delta.y)),chaseSpeed*-1);
                }
                if (collected.timeUntilExplosion() < throwRemainingTime || holdTimer >= holdDuration) {
                    target = minPlayer;
                    timer = 0;
                    nextAnimation = "throwing";
                    getFanta().rewindCurrentAnimation();
                    if((Math.sin(Math.atan2(delta.x, delta.y)) * throwSpeed) > 0){
                        setFlipX(true);
                    }else{
                        setFlipX(false);
                    }
                    timer = 0;
                    throwing = true;
                }
            }
        }else if(timer > cooldown){
            float minDist = Float.MAX_VALUE;
            Bomb nearestBomb = null;
            for (int i = 0; i < man.getEntities().size(); i++) {
                Entity ent2 = man.getEntities().get(i);
                if(ent2 instanceof Bomb && ((Bomb)ent2).isPickedUpBy() == null){
                    float dist = ent2.getCenter().dst(getCenter());
                    if(dist < minDist){
                        minDist = dist;
                        nearestBomb = (Bomb)ent2;
                    }
                }
            }
            if(nearestBomb != null && minDist < aggroRange){
                nextAnimation = "chasing";
                if(nearestBomb.timeUntilExplosion() > throwRemainingTime/2) {
                    Vector2 delta = nearestBomb.getCenter().sub(getCenter());
                    move((float) Math.atan2(delta.x, delta.y), collectSpeed);
                }else{
                    Vector2 delta = nearestBomb.getCenter().sub(getCenter());
                    move((float) Math.atan2(delta.x, delta.y), collectSpeed * -1);
                }
            }
        }
        if(getFanta().getCurrentAnimation().equals("standing") && !nextAnimation.equals("standing")){
            setSpawnProtection(2);
        }
        timer++;
        getFanta().setCurrentAnimation(nextAnimation);
    }

    @Override
    public void die(EntityManager man) {
        super.die(man);
        if(collected != null){
            collected.throwAway();
            collected = null;
        }
    }

    @Override
    public boolean collidesWith(Entity other) {
        if(super.collidesWith(other) && other != collected){
            return true;
        }
        return false;
    }

    @Override
    public void interactWith(Entity ent2, Collision col, EntityManager man) {
        if(col != null && ent2 instanceof Bomb && timer > cooldown && ((Bomb)ent2).isPickedUpBy() == null){
            if(collected != null){
                collected.throwAway();
            }
            collected = (Bomb)ent2;
            ((Bomb) ent2).pickUp(this);
            holdTimer = 0;
        }
    }

    @Override
    protected void randomMovementBehaviour() {
        if(timer > cooldown && collected == null) {
            nextAnimation = "standing";
            super.randomMovementBehaviour();
        }
    }
}
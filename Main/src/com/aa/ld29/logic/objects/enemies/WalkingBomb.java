package com.aa.ld29.logic.objects.enemies;

import com.aa.ld29.dal.audio.SoundFX;
import com.aa.ld29.dal.audio.SoundManager;
import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.Fanta;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 29.07.2014.
 */
public class WalkingBomb extends Enemy {

    private float aggroRange = 100;
    private float moveSpeed = 1;
    private float aggroDelay = 36;
    private float aggroTimer;
    private float jumpSpeed = 1.8f;
    private float fuseTimer;
    private float fuseDuration = 111;
    private float radius = 50;
    private float damage = 1;
    private Entity target;

    private String[] explosionSounds = SoundFX.LIGHT_EXPLOSIONS;

    public WalkingBomb(Fanta f) {
        super(f);
        setBounZ(0.1f);
        setCollisionDamage(0);
    }

    @Override
    public void act(EntityManager man) {
        String nextAnimation = getFanta().getCurrentAnimation();
        super.act(man);
        if(getFanta().getCurrentAnimation().equals("standing")){
            for (int i = 0; i < man.getAliveTargets().size(); i++) {
                Entity player = man.getAliveTargets().get(i);
                float dist = player.getCenter().dst(getCenter());
                if (dist < aggroRange) {
                    target = player;
                    nextAnimation = "jumping";
                    setAwesomeEffect(true);
                    setSpeedZ(jumpSpeed);
                    break;
                }
            }
        }else if(getFanta().getCurrentAnimation().equals("jumping")){
            if(aggroTimer >= aggroDelay) {
                nextAnimation="chasing";
            }else{
                aggroTimer++;
            }
        }else if(getFanta().getCurrentAnimation().equals("chasing")){
            if(fuseTimer < fuseDuration) {
                Vector2 delta = target.getCenter().sub(getCenter());
                move((float) Math.atan2(delta.x, delta.y), moveSpeed);
                fuseTimer++;
            }else{
                setMaxHealth(0);
                explode(man);
            }
        }
        getFanta().setCurrentAnimation(nextAnimation);
    }

    @Override
    public void die(EntityManager man) {
        super.die(man);
        explode(man);
    }

    private void explode(EntityManager man){
        //GO BOOM
        SoundManager.getInstance().playRandom(explosionSounds);
        man.createExplosion(getCenter().x,getCenter().y,damage,radius);
    }

    @Override
    protected void randomMovementBehaviour() {
        //Do nothing
    }
}

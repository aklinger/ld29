package com.aa.ld29.logic.objects.enemies;

import com.aa.ld29.logic.Entity;

/**
 * Created by Toni on 28.04.2014.
 */
public interface Puncher {
    public boolean canPunchInTheFace(Entity ent);
}

package com.aa.ld29.logic.objects.enemies;

import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.Fanta;
import com.aa.ld29.logic.objects.Jumpy;
import com.aa.ld29.maths.Collision;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 26.04.2014.
 */
public class Enemy extends Jumpy {

    private float moveIntervall = 60;
    private float moveImpulse = 0.8f;
    private float moveTimer = 0;

    private float pushback = 1;
    private float collisionDamage = 1;
    private float pointsWorth = 10;

    public Enemy (Fanta f){
        super(f);
        setAwesomeEffect(false);
        setFx(0.99f);
        setFy(0.99f);
    }

    @Override
    public void act(EntityManager man) {
        super.act(man);
        randomMovementBehaviour();
    }

    protected void randomMovementBehaviour(){
        moveTimer++;
        if(moveTimer > moveIntervall){
            float dir = (float)(Math.random()*Math.PI*2);
            this.addSpeed((float) Math.sin(dir) * moveImpulse, (float) Math.cos(dir) * moveImpulse);
            moveTimer = 0;
        }
    }

    @Override
    public void interactWith(Entity ent2, Collision col, EntityManager man) {
        if(col != null && ent2.collidesWith(this)){
            biteEntity(ent2);
        }
    }

    protected void move(float direction, float speed){
        float movex = (float)(Math.sin(direction)*speed);
        addPosition(movex,(float)(Math.cos(direction)*speed));
        if(movex > 0){
            setFlipX(true);
        }else{
            setFlipX(false);
        }
    }

    protected void biteEntity(Entity entity){
        if(collisionDamage > 0) {
            entity.damage(collisionDamage);
            Vector2 delta = entity.getCenter().sub(this.getCenter());
            entity.addSpeed(delta.nor().scl(pushback));
        }
    }

    @Override
    public boolean collidesWith(Entity other) {
        if(super.collidesWith(other) && (!this.isUnderground() || other instanceof Enemy)){
            return true;
        }
        return false;
    }

    @Override
    public void die(EntityManager man) {
        super.die(man);
        man.addScore((int)pointsWorth);
    }

    public void setPushback(float pushback) {
        this.pushback = pushback;
    }

    public void setCollisionDamage(float collisionDamage) {
        this.collisionDamage = collisionDamage;
    }

    public boolean isUnderground() {
        return this.getFanta().getCurrentAnimation().equals("standing");
    }

    public void setPointsWorth(float pointsWorth) {
        this.pointsWorth = pointsWorth;
    }
}

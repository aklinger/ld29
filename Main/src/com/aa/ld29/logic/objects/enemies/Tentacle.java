package com.aa.ld29.logic.objects.enemies;

import com.aa.ld29.dal.audio.SoundFX;
import com.aa.ld29.dal.audio.SoundManager;
import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.Fanta;
import com.aa.ld29.logic.objects.bombs.Bomb;

/**
 * Created by Toni on 27.04.2014.
 */
public class Tentacle extends Enemy implements Puncher {

    private float attackCooldown = 60*1.2f;
    private float attackPoint = 7f;
    private float aggroRange = 62;
    private float punchRange = 35f;
    private float hitRange = 36f;
    private float hitDamage = 2;
    private float pushBack = 3f;
    private boolean attacking;
    private boolean waiting;

    private float attackTimer = attackCooldown;

    public Tentacle(Fanta f) {
        super(f);
        setCollisionDamage(0);
        setFx(0.95f);
        setFy(0.95f);
    }

    String nextAnimation;

    @Override
    public void act(EntityManager man) {
        nextAnimation = getFanta().getCurrentAnimation();
        super.act(man);
        waiting = false;
        if(attackTimer > attackCooldown) {
            float minDist = Float.MAX_VALUE;
            Entity nearestThing = null;
            for (int i = 0; i < man.getAliveTargets().size(); i++) {
                Entity player = man.getAliveTargets().get(i);
                float dist = player.getCenter().dst(getCenter());
                if (dist < aggroRange && dist < minDist) {
                    minDist = dist;
                    nearestThing = player;
                }
            }
            if(minDist >= punchRange){
                for (int i = 0; i < man.getEntities().size(); i++) {
                    Entity ent2 = man.getEntities().get(i);
                    if(ent2 instanceof Bomb){
                        float dist = ent2.getCenter().dst(getCenter());
                        if(dist < aggroRange){
                            minDist = dist;
                            nearestThing = ent2;
                            if(minDist < punchRange) {
                                break;
                            }
                        }
                    }
                }
            }
            if (minDist < aggroRange) {
                if(nearestThing.getX()-getX() > 0){
                    setFlipX(true);
                }else{
                    setFlipX(false);
                }
                waiting = true;
                if(getFanta().getCurrentAnimation().equals("standing")) {
                    nextAnimation = "emerging";
                    getFanta().rewindCurrentAnimation();
                    setSx(getSx() * 0.01f);
                    setSy(getSy() * 0.01f);
                    setFixated(true);
                }else if(minDist < punchRange && getFanta().getCurrentAnimation().equals("normal")){
                    attackTimer = 0;
                    attacking = true;
                    nextAnimation = "attacking";
                    getFanta().rewindCurrentAnimation();
                }else if(getFanta().getCurrentAnimation().equals("emerging")){
                    nextAnimation = "emerging";
                }
            }
        }
        if(attacking && attackTimer > attackPoint){
            //create HitBox that hurts people
            SoundManager.getInstance().play(SoundFX.HURT_1);
            man.punchPeopleInTheFace(getCenter().x,getCenter().y,hitRange,hitDamage,pushBack,this);
            attacking = false;
        }
        attackTimer++;
        getFanta().setCurrentAnimation(nextAnimation);
    }

    @Override
    protected void randomMovementBehaviour() {
        if(!attacking && !waiting && attackTimer > attackCooldown * 1.5f) {
            if(getFanta().getCurrentAnimation().equals("normal") || getFanta().getCurrentAnimation().equals("emerging"))
            {
                nextAnimation = "digging";
                getFanta().rewindCurrentAnimation();
            }else if(getFanta().isAnimationFinished()){
                nextAnimation = "standing";
                setFixated(false);
            }
            if(!isFixated()){
                super.randomMovementBehaviour();
            }
        }else if(getFanta().isAnimationFinished()){
            getFanta().setCurrentAnimation("normal");
            nextAnimation = "normal";
        }
    }

    @Override
    public boolean canPunchInTheFace(Entity ent) {
        return !(ent instanceof Tentacle);
    }

    public void setPunchRange(float punchRange) {
        this.punchRange = punchRange;
    }

    public void setHitRange(float hitRange) {
        this.hitRange = hitRange;
    }

    public void setHitDamage(float hitDamage) {
        this.hitDamage = hitDamage;
    }

    public void setAggroRange(float aggroRange) {
        this.aggroRange = aggroRange;
    }
}

package com.aa.ld29.logic.objects.enemies.bossfights;

/**
 * Created by Toni on 28.04.2014.
 */
public class DigdriSetting {
    public int shotType = 0;
    public float aggroRange = 0;
    public float cooldown = 0;
    public float streuung = 0;

    public static final DigdriSetting NONE = new DigdriSetting();

    public DigdriSetting() {

    }

    public DigdriSetting(int shotType, float aggroRange, float cooldown, float streuung) {
        this.shotType = shotType;
        this.aggroRange = aggroRange;
        this.cooldown = cooldown;
        this.streuung = streuung;
    }
}

package com.aa.ld29.logic.objects.enemies.bossfights;

import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.Fanta;
import com.aa.ld29.logic.objects.enemies.Digdri;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Toni on 28.04.2014.
 */
public class DigdriBoss extends Digdri {

    public List<DigdriSetting> settings;
    private float changeCooldown = 60f*3;
    private float modeDuration = 60f*5;
    private float changeTimer = modeDuration;
    private int currentMode = 0;

    public DigdriBoss(Fanta f) {
        super(f);
        settings = new ArrayList<>();
        setSetting(DigdriSetting.NONE);
        setExtraShots(0);
    }

    @Override
    public void act(EntityManager man) {
        super.act(man);
        changeTimer++;
        if(changeTimer > changeCooldown + modeDuration){
            changeTimer = 0;
            currentMode++;
            if(currentMode>=settings.size()){
                currentMode = 0;
            }
            setSetting(settings.get(currentMode));
        }else if(changeTimer > modeDuration){
            setSetting(DigdriSetting.NONE);
        }
    }

    public void addSetting(DigdriSetting set) {
        settings.add(set);
    }
}

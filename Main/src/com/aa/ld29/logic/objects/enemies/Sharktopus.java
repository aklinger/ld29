package com.aa.ld29.logic.objects.enemies;

import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.Fanta;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 26.04.2014.
 */
public class Sharktopus extends Enemy{

    private float aggroRange = 120;
    private float chaseAcceleration1 = 0.05f;
    private float chaseAcceleration2 = 0.09f;
    private float chaseMaxSpeed = 2.5f;
    private float cooldown = 60*1f;
    private float biteRange = 42;

    private float timer = 0;

    private boolean chasing = false;

    public Sharktopus (Fanta f){
        super(f);
        setFx(0.985f);
        setFy(0.985f);
        setCollisionDamage(1);
        setPushback(2);
    }
    @Override
    public void act(EntityManager man) {
        String nextAnimation = getFanta().getCurrentAnimation();
        super.act(man);

        if(!getFanta().getCurrentAnimation().equals("biting") || getFanta().isAnimationFinished()) {
            nextAnimation = "standing";
        }
        for (int i = 0; i < man.getAliveTargets().size(); i++) {
            Entity player = man.getAliveTargets().get(i);
            float dist = player.getCenter().dst(getCenter());
            if (dist < aggroRange) {
                Vector2 delta = player.getCenter().sub(getCenter());
                chasing = true;
                nextAnimation = "chasing";
                if(timer > cooldown) {
                    if(dist < biteRange){
                        nextAnimation = "biting";
                        move((float)Math.atan2(delta.x,delta.y), chaseAcceleration2);
                    }else{
                        move((float)Math.atan2(delta.x,delta.y), chaseAcceleration1);
                    }
                }else{
                    move((float)Math.atan2(delta.x,delta.y), chaseAcceleration2 *-0.5f);
                }
                break;
            }
        }
        timer++;
        getFanta().setCurrentAnimation(nextAnimation);
    }

    @Override
    protected void move(float direction, float speed){
        float movex = (float)(Math.sin(direction)*speed);
        addSpeed(movex, (float) (Math.cos(direction) * speed));
        Vector2 velocity = new Vector2(getSx(),getSy());
        if(velocity.len()>chaseMaxSpeed){
            velocity.nor().scl(chaseMaxSpeed);
            setSx(velocity.x);
            setSy(velocity.y);
        }
        if(movex > 0){
            setFlipX(true);
        }else{
            setFlipX(false);
        }
    }

    @Override
    protected void biteEntity(Entity entity) {
        if(getFanta().getCurrentAnimation().equals("biting")) {
            super.biteEntity(entity);
            timer = 0;
        }
    }

    @Override
    protected void randomMovementBehaviour() {
        if(timer > cooldown && !chasing) {
            super.randomMovementBehaviour();
        }
        chasing = false;
    }

    @Override
    public int getType() {
        return CIRCLE;
    }

    public void setAggroRange(float aggroRange) {
        this.aggroRange = aggroRange;
    }

    public void setChaseAcceleration1(float chaseAcceleration1) {
        this.chaseAcceleration1 = chaseAcceleration1;
    }

    public void setChaseAcceleration2(float chaseAcceleration2) {
        this.chaseAcceleration2 = chaseAcceleration2;
    }

    public void setChaseMaxSpeed(float chaseMaxSpeed) {
        this.chaseMaxSpeed = chaseMaxSpeed;
    }

    public void setBiteRange(float biteRange) {
        this.biteRange = biteRange;
    }

    public void setCooldown(float cooldown) {
        this.cooldown = cooldown;
    }
}

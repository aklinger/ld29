package com.aa.ld29.logic.objects;

import com.aa.ld29.dal.audio.SoundFX;
import com.aa.ld29.dal.audio.SoundManager;
import com.aa.ld29.logic.BasicObject;
import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.Fanta;
import com.aa.ld29.maths.Collision;

/**
 * Created by Toni on 26.04.2014.
 */
public class Goodie extends BasicObject{

    private int score = 0;
    private int healing = 0;
    private int ammo = 0;
    private int ammoType = 0;

    private boolean alive = true;

    public Goodie (Fanta f){
        super(f);
        setSolid(false);
    }

    @Override
    public void interactWith(Entity ent2, Collision col, EntityManager man) {
        if( col != null && ent2 instanceof Spelunker){
            SoundManager.getInstance().play(SoundFX.PICKUP);
            ent2.heal(healing);
            man.addScore(score);
            if(ammo > 0) {
                ((Spelunker) ent2).addAmmo(ammo, ammoType);
                ((Spelunker) ent2).setCurrentSpecial(ammoType);
            }
            alive = false;
        }
    }

    @Override
    public void damage(float damage) {
        //Nothing
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setHealing(int healing) {
        this.healing = healing;
    }

    public void setAmmo(int ammo, int ammoType) {
        this.ammo = ammo;
        this.ammoType = ammoType;
    }
}

package com.aa.ld29.logic.objects;

import com.aa.ld29.logic.BasicObject;
import com.aa.ld29.logic.Entity;
import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.Fanta;
import com.aa.ld29.maths.Collision;

/**
 * Created by Toni on 26.04.2014.
 */
public class Door extends BasicObject{
    public Door(Fanta f){
        super(f);
        this.setSolid(false);
    }

    @Override
    public void damage(float damage) {
        //it's the exit, it shouldn't be able to get damaged
    }

    @Override
    public boolean collidesWith(Entity other) {
        return other instanceof Spelunker;
    }

    @Override
    public void interactWith(Entity ent2, Collision col, EntityManager man) {
        if(ent2 instanceof Spelunker && col != null){
            ((Spelunker) ent2).setFixedInScene(true);
            ((Spelunker) ent2).setTargetPosition(this.getCenter());
            man.triggerLevelComplete();
        }
    }
}

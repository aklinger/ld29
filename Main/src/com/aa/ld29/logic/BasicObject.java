package com.aa.ld29.logic;

import com.aa.ld29.dal.audio.SoundFX;
import com.aa.ld29.dal.audio.SoundManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Toni on 26.04.2014.
 */
public class BasicObject extends Entity {

    private float hurtDuration = 25;
    private float hurtTimer = hurtDuration;

    private String hurtSound = SoundFX.HURT_4;

    public BasicObject (Fanta fanta){
        setFanta(fanta, true, true);
    }

    @Override
    public void act(EntityManager man) {
        getFanta().act(1);
        computeMovement();
        hurtTimer++;
    }

    @Override
    public void damage(float damage) {
        if(!isHurt()) {
            SoundManager.getInstance().play(hurtSound);
            super.damage(damage);
            hurtTimer = 0;
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        if (!isHurt() || hurtTimer % 2 == 0) {
            super.render(batch);
        }
    }

    public boolean isHurt(){
        return hurtTimer < hurtDuration;
    }

    public void setHurtSound(String hurtSound) {
        this.hurtSound = hurtSound;
    }
}

package com.aa.ld29.logic.particle;

import com.aa.ld29.logic.EntityManager;
import com.aa.ld29.logic.Fanta;
import com.aa.ld29.logic.objects.Jumpy;

/**
 * Created by Toni on 27.04.2014.
 */
public class AnimatedParticle extends Jumpy implements Particle{

    private float duration = -1;
    private float time = 0;

    public AnimatedParticle (Fanta f){
        super(f);
    }

    @Override
    public void act() {
        super.act(null);
        getFanta().act(1);
        computeMovement();
        time++;
    }

    @Override
    public boolean isActive() {
        if(duration < 0 || time>duration){
            return !getFanta().isAnimationFinished();
        }
        return true;
    }

    @Override
    public void act(EntityManager man) {
        act();
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }
}

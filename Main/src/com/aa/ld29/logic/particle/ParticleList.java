package com.aa.ld29.logic.particle;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Manages a lot of Particles (stuff that only looks pretty) with an ArrayList.
 * <p/>
 * It maybe doesn't do this the best way, but if it's a bottleneck, you can
 * write your own implementation.
 * <p/>
 * Created by Toni on 12.07.2014.
 */
public class ParticleList implements ParticleManager {
    //Attribute
    private final ArrayList<Particle> particles;

    //Konstruktor

    /**
     * Creates a new ParticleList using a simple ArrayList.
     */
    public ParticleList() {
        particles = new ArrayList<>();
    }

    //Methoden
    @Override
    public void addParticle(Particle particle) {
        particles.add(particle);
    }

    @Override
    public void addAllParticles(Collection<Particle> particles) {
        this.particles.addAll(particles);
    }

    @Override
    public void act() {
        List<Particle> trash = new ArrayList<Particle>();
        for (Particle part : particles) {
            part.act();
            if (!part.isActive()) {
                trash.add(part);
            }
        }
        if (!trash.isEmpty()) {
            particles.removeAll(trash);
            particles.trimToSize();
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        for (Particle part : particles) {
            part.render(batch);
        }
    }
}


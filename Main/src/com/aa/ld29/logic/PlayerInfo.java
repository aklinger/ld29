package com.aa.ld29.logic;

/**
 * Created by Toni on 27.04.2014.
 */
public class PlayerInfo {
    public float hp;
    public int score;
    public int[] specialAmmo;
    public int currentSpecial;
    public int deaths;

    public PlayerInfo(float hp, int score, int[] specialAmmo) {
        this.hp = hp;
        this.score = score;
        this.specialAmmo = specialAmmo;
        this.currentSpecial = 0;
        this.deaths = 0;
    }
}

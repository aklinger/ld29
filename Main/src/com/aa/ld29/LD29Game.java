package com.aa.ld29;

import com.aa.ld29.dal.BelowEarthPreferences;
import com.aa.ld29.dal.TextureManager;
import com.aa.ld29.gui.MainMenuScreen;
import com.aa.ld29.log.MyLogger;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.io.File;
import java.io.IOException;

/**
 * Created by Toni on 12.04.2014.
 */
public class LD29Game extends Game{
    //Attributes
    private static SpriteBatch batch;
    private static Skin skin;
    private static MainMenuScreen mainMenu;

    public static String pathOfJar;
    public static final boolean DEVELOPER_MODE = true;

    public static final String MUSIC_MAIN_MENU = null;
    public static final String MUSIC_GAMEPLAY = MUSIC_MAIN_MENU;

    //Methods
    @Override
    public void create() {
        MyLogger.log("Started Game");
        /*if(developerMode){
            pathOfJar = getJarFolder();
            System.out.println("");
            System.out.println("Path of this jar: "+pathOfJar);
            System.out.println("");

            try {
                FileHandle fh = Gdx.files.absolute(pathOfJar+"assets/Put_stuff_here.txt");
                try (OutputStream write = fh.write(false)) {
                    write.write((int) Math.random());
                }
            } catch (IOException ex) {
                Logger.getLogger(OctoberChallenge13.class.getName()).log(Level.SEVERE, null, ex);
            }
        }*/

        BelowEarthPreferences.getInstance().loadSettings();

        batch = new SpriteBatch();
        skin = new Skin(Gdx.files.internal("gfx/gui/uiskin.json"));
        try {
            TextureManager.getInstance().loadTextures("texture_data.res");
        } catch (IOException ex) {
            MyLogger.log("Error loading textures", ex);
        }
        mainMenu = new MainMenuScreen(skin, batch, this);
        goToMainMenuScreen();
    }

    public void goToMainMenuScreen(){
        /*MusicManager.getInstance().playIfNotAlreadyPlaying(LD29Game.MUSIC_MAIN_MENU);*/
        setScreen(mainMenu);
    }

    /**
     * http://stackoverflow.com/questions/320542/how-to-get-the-path-of-a-running-jar-file
     * @return The path of the folder containing the jar running this program.
     */
    private String getJarFolder() {
        // get name and path
        String name = getClass().getName().replace('.', '/');
        name = getClass().getResource("/" + name + ".class").toString();
        // remove junk
        int indexOfJar = name.indexOf(".jar");
        if(indexOfJar != -1){
            name = name.substring(0, indexOfJar);
            name = name.substring(name.lastIndexOf(':')-1, name.lastIndexOf('/')+1).replace('%', ' ');
        }
        // remove escape characters
        String s = "";
        for (int k=0; k<name.length(); k++) {
            s += name.charAt(k);
            if (name.charAt(k) == ' ') k += 2;
        }
        // replace '/' with system separator char
        return s.replace('/', File.separatorChar);
    }
}

# Below Earth - Ludum Dare 29 #

### What is this repository for? ###

This is my entry for Ludum Dare #29 where the theme was 'Beneath the Surface', and the game is called 'Below Earth'. We decided to develop it further and create a post-compo version. 

Below Earth is a top-down arcade game, not unlike BomberMan but still a lot different. It is about running around in a cave, throwing around bombs, destroying walls and killing enemies before they kill you. 
This is not always an easy task, as they will resort to tricks like throwing back your bombs or hiding underground.

http://www.ludumdare.com/compo/ludum-dare-29/?action=preview&uid=20290

The original Ludum Dare Entry is available from the Bitbucket downloads page.

https://bitbucket.org/aklinger/ld29/downloads

Builds of future versions will be available there too, every now and then.

### How do I get set up? ###

Either download a binary, or clone the repo and build the project from source yourself.

The project is being developed using LibGDX (1.2.0) and IntelliJ IDEA.

### Contribution guidelines ###

Basically you can fork the source code to fix features and implement bugs (or the other way round) as much as you like to. 
If it makes sense to include it you can make a pull request, and we will probably accept it.

### Who do I talk to? ###

If you want to know more about the project, want to help or simply complain, just talk to the repo owner: 
https://bitbucket.org/account/notifications/send/?receiver=klianc09

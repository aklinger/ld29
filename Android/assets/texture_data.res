$folder:gfx/bg
bg.png:main_menu_bg

$folder:gfx/tiles
FloorTile.png:tile_floor
OreTile.png:tile_ore
RockTile_Sheet.png:tile_rock
LavaTile.png:tile_lava
LavaRockTile.png:tile_lavarock
BedrockTile_Sheet.png:tile_bedrock

$folder:gfx/objects

Exit.png:door

Player_Sheet.png:player_standing

Heart.png:heart
Dynamite_Icon.png:dynamite_icon
Cluster.png:cluster_icon
Schnirdlkrocha_Icon.png:schnirdlkrocha_icon
Mine_Icon.png:mine_icon
StickyBomb_Sheet.png:sticky_icon

Bomb_Sheet.png:bomb_burning
Dynamite_Sheet.png:dynamite_burning
Cluster.png:cluster_burning
Shrapnel.png:shrapnel
Schnirdlkrocha_Sheet.png:schnirdlkrocha_burning
Mine_Sheet.png:mine_bomb
StickyBomb_Sheet.png:sticky_bomb

Crystal.png:crystal
Healthpack.png:healthpack
Crate_Dynamite.png:ammopack_dynamite
Crate_Cluster.png:ammopack_cluster

Rubbish_Sheet.png:rubbish
Monster.png:monster
Spit.png:spit
Digdri_Sheet.png:digdri
MoleShark_Sheet.png:shark
Thief_Sheet.png:thief
Tentacle_Sheet.png:tentacle
Octohead_Sheet.png:octohead
Walking_Bomb_Sheet.png:walking_bomb

$folder:gfx/particles

Explosion_Sheet.png:explosion
Pebble_Sheet.png:pebbles
Shadow.png:shadow
Shadow2.png:shadow2

package com.aa.ld29;

import com.aa.ld29.log.MyLogger;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import javax.swing.*;
import java.util.logging.Level;

/**
 * Created by Toni on 12.04.2014.
 */
public class DesktopStarter {
    public static void main(String[] args) {
        try {
            LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
            cfg.title = "Below Earth";
            cfg.useGL30 = false;
            cfg.width = 800;
            cfg.height = 600;
            new LwjglApplication(new LD29Game(), cfg);
        } catch (Exception ex){
            JOptionPane.showMessageDialog(null,"A severe error occured\n: "+ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
    }
}
